﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FPGA_mGUI
{
    public class AutoCompleteUserInputsAddress
    {
        public string FILENAME;
        List<string> userInputAddress = new List<string>();
        string startPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);


        public AutoCompleteUserInputsAddress(string fileName)
        {
            this.FILENAME = fileName;
        }


        public void Open()
        {
            if (!File.Exists($"{startPath}\\{FILENAME}"))
            {
                File.Create($"{startPath}\\{FILENAME}");
            }
        }

        public string[] returnListUserInputAddress()
        {
            using (StreamReader sr = new StreamReader($"{startPath}\\{FILENAME}"))
            {
                string[] lines = sr.ReadToEnd().Trim().Split('\n');
                return lines;
            }
        }
        public void AddValue(string value)
        {
            string[] lines;
            Boolean isExist = false;
            using (StreamReader sr = new StreamReader($"{startPath}\\{FILENAME}"))
            {
                lines = sr.ReadToEnd().Trim().Split('\n');
                sr.Close();
            }
            foreach (string line in lines)
            {
                if (line.Equals(value))
                {
                    isExist = true;
                    break;
                }
            }

            WriteIfNotExist(isExist, value);
        }

        public void WriteIfNotExist(Boolean isExist, string value)
        {
            if (!isExist)
            {
                using (StreamWriter sw = File.AppendText($"{startPath}\\{FILENAME}"))
                {
                    sw.WriteLine(value);
                    sw.Flush();
                    sw.Close();
                }
            }
        }
    }
}
