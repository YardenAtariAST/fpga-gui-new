﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPGA_mGUI
{

    public class Block
    {
        public enum eBlockNames
        {
            BF,
            CALIB,
            COMM_HUB,
            TOP,
            DFE0,
            DFE1,
            DFE2,
            DFE3,
            DFE4,
            DFE5,
            DFE6,
            DFE7,
            DFE8,
            DFE9,
            DFE10,
            DFE11,
            DFE12,
            DFE13,
            DFE14,
            DFE15
        }

        private readonly eBlockNames blockName;
        private readonly List<Register> registersList= new List<Register>();
        private readonly int baseAddress;
        private string rxORtx;

        public Block(eBlockNames m_blockName, List<Register> m_registersList, int m_baseAddress)
        {
            blockName = m_blockName;
            registersList = m_registersList;
            baseAddress = m_baseAddress;
        }

        public eBlockNames BlockName
        {
            get
            {
                return blockName;
            }
        }
        public List<Register> RegistersList
        {
            get
            {
                return registersList;
            }
            
        }

        public string RxOrtx
        {
            get
            {
                return rxORtx;
            }
            set
            {
                this.rxORtx = value.ToString();
            }
        }

        public int BaseAddress
        {
            get
            {
                return baseAddress;
            }
        }

    }
}
