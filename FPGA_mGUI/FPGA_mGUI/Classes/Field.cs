﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPGA_mGUI.Classes
{
    public class Field
    {
        public string fieldName { get; set; }
        public int fieldBits { get; set; }
        public int startIndex { get; set; }
        public int endIndex { get; set; }
        public string fieldValue { get; set; }

        public Field(string m_fieldName, int m_fieldBits, int m_startIndex, int m_endIndex, string m_fieldValue)
        {
            fieldName = m_fieldName;
            fieldBits = m_fieldBits;
            startIndex = m_startIndex;
            endIndex = m_endIndex;
            fieldValue = m_fieldValue;
        }

    }
}
