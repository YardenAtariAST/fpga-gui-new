﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FPGA_mGUI.Classes;

namespace FPGA_mGUI
{
    public class Register
    {
        public Block.eBlockNames blockName { get; set; }

        public string registerName { get; set; }
        public int addressOffset { get; set; }
        public string fullValue { get; set; }

        public readonly List<Field> fieldsList = new List<Field>();

        public Register(string m_registerName, Block.eBlockNames m_blockName, int m_addressOffset, string m_fullValue, List<Field> m_fieldsList)
        {
            blockName = m_blockName;
            addressOffset = m_addressOffset;
            fullValue = m_fullValue;
            fieldsList = m_fieldsList;
            registerName = m_registerName;
        }

        public List<Field> FieldsList
        {
            get
            {
                return fieldsList;
            }
        }

    }
}
