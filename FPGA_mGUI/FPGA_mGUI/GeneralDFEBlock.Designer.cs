﻿namespace FPGA_mGUI
{
    partial class GeneralDFEBlock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanelTX = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanelRX = new System.Windows.Forms.FlowLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.writeAllTXBtn = new System.Windows.Forms.Button();
            this.readAllTXBtn = new System.Windows.Forms.Button();
            this.txlbl = new System.Windows.Forms.Label();
            this.writeAllRXBtn = new System.Windows.Forms.Button();
            this.readAllRXBtn = new System.Windows.Forms.Button();
            this.rxlbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 81);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanelTX);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanelRX);
            this.splitContainer1.Size = new System.Drawing.Size(1586, 780);
            this.splitContainer1.SplitterDistance = 787;
            this.splitContainer1.TabIndex = 0;
            // 
            // flowLayoutPanelTX
            // 
            this.flowLayoutPanelTX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanelTX.AutoScroll = true;
            this.flowLayoutPanelTX.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelTX.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanelTX.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelTX.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanelTX.Name = "flowLayoutPanelTX";
            this.flowLayoutPanelTX.Size = new System.Drawing.Size(787, 780);
            this.flowLayoutPanelTX.TabIndex = 12;
            // 
            // flowLayoutPanelRX
            // 
            this.flowLayoutPanelRX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanelRX.AutoScroll = true;
            this.flowLayoutPanelRX.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelRX.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanelRX.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelRX.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanelRX.Name = "flowLayoutPanelRX";
            this.flowLayoutPanelRX.Size = new System.Drawing.Size(795, 780);
            this.flowLayoutPanelRX.TabIndex = 12;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(12, 12);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.writeAllTXBtn);
            this.splitContainer2.Panel1.Controls.Add(this.readAllTXBtn);
            this.splitContainer2.Panel1.Controls.Add(this.txlbl);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.writeAllRXBtn);
            this.splitContainer2.Panel2.Controls.Add(this.readAllRXBtn);
            this.splitContainer2.Panel2.Controls.Add(this.rxlbl);
            this.splitContainer2.Size = new System.Drawing.Size(1586, 63);
            this.splitContainer2.SplitterDistance = 787;
            this.splitContainer2.TabIndex = 1;
            // 
            // writeAllTXBtn
            // 
            this.writeAllTXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.writeAllTXBtn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.writeAllTXBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.writeAllTXBtn.Location = new System.Drawing.Point(293, 11);
            this.writeAllTXBtn.Margin = new System.Windows.Forms.Padding(4);
            this.writeAllTXBtn.Name = "writeAllTXBtn";
            this.writeAllTXBtn.Size = new System.Drawing.Size(167, 45);
            this.writeAllTXBtn.TabIndex = 13;
            this.writeAllTXBtn.Text = "Write All";
            this.writeAllTXBtn.UseVisualStyleBackColor = false;
            this.writeAllTXBtn.Click += new System.EventHandler(this.writeAllTXBtn_Click);
            // 
            // readAllTXBtn
            // 
            this.readAllTXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.readAllTXBtn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readAllTXBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.readAllTXBtn.Location = new System.Drawing.Point(118, 11);
            this.readAllTXBtn.Margin = new System.Windows.Forms.Padding(4);
            this.readAllTXBtn.Name = "readAllTXBtn";
            this.readAllTXBtn.Size = new System.Drawing.Size(167, 45);
            this.readAllTXBtn.TabIndex = 12;
            this.readAllTXBtn.Text = "Read All";
            this.readAllTXBtn.UseVisualStyleBackColor = false;
            this.readAllTXBtn.Click += new System.EventHandler(this.readAllTXBtn_Click);
            // 
            // txlbl
            // 
            this.txlbl.AutoSize = true;
            this.txlbl.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txlbl.Location = new System.Drawing.Point(8, 11);
            this.txlbl.Name = "txlbl";
            this.txlbl.Size = new System.Drawing.Size(58, 43);
            this.txlbl.TabIndex = 11;
            this.txlbl.Text = "TX";
            // 
            // writeAllRXBtn
            // 
            this.writeAllRXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.writeAllRXBtn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.writeAllRXBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.writeAllRXBtn.Location = new System.Drawing.Point(294, 9);
            this.writeAllRXBtn.Margin = new System.Windows.Forms.Padding(4);
            this.writeAllRXBtn.Name = "writeAllRXBtn";
            this.writeAllRXBtn.Size = new System.Drawing.Size(167, 45);
            this.writeAllRXBtn.TabIndex = 16;
            this.writeAllRXBtn.Text = "Write All";
            this.writeAllRXBtn.UseVisualStyleBackColor = false;
            this.writeAllRXBtn.Click += new System.EventHandler(this.writeAllRXBtn_Click);
            // 
            // readAllRXBtn
            // 
            this.readAllRXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.readAllRXBtn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readAllRXBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.readAllRXBtn.Location = new System.Drawing.Point(119, 9);
            this.readAllRXBtn.Margin = new System.Windows.Forms.Padding(4);
            this.readAllRXBtn.Name = "readAllRXBtn";
            this.readAllRXBtn.Size = new System.Drawing.Size(167, 45);
            this.readAllRXBtn.TabIndex = 15;
            this.readAllRXBtn.Text = "Read All";
            this.readAllRXBtn.UseVisualStyleBackColor = false;
            this.readAllRXBtn.Click += new System.EventHandler(this.readAllRXBtn_Click);
            // 
            // rxlbl
            // 
            this.rxlbl.AutoSize = true;
            this.rxlbl.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rxlbl.Location = new System.Drawing.Point(9, 9);
            this.rxlbl.Name = "rxlbl";
            this.rxlbl.Size = new System.Drawing.Size(64, 43);
            this.rxlbl.TabIndex = 14;
            this.rxlbl.Text = "RX";
            // 
            // GeneralDFEBlock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1610, 873);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.splitContainer1);
            this.Name = "GeneralDFEBlock";
            this.Text = "GeneralDFEBlock";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button writeAllTXBtn;
        private System.Windows.Forms.Button readAllTXBtn;
        private System.Windows.Forms.Label txlbl;
        private System.Windows.Forms.Button writeAllRXBtn;
        private System.Windows.Forms.Button readAllRXBtn;
        private System.Windows.Forms.Label rxlbl;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelTX;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelRX;
    }
}