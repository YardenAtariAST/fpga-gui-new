﻿namespace FPGA_mGUI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.terminalTextBox = new System.Windows.Forms.RichTextBox();
            this.clearTerminalBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.NtoStartModetb = new System.Windows.Forms.TextBox();
            this.StartModeBtn = new System.Windows.Forms.Button();
            this.resetFPGAbtn = new System.Windows.Forms.Button();
            this.resetSPIbtn = new System.Windows.Forms.Button();
            this.sendFileBtn = new System.Windows.Forms.Button();
            this.sendTextBtn = new System.Windows.Forms.Button();
            this.sendFileTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.LoadTextFileBtn = new System.Windows.Forms.Button();
            this.sendTextTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.OpenClosePortBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.BitsPerSecondsComboBox = new System.Windows.Forms.ComboBox();
            this.ComComboBox = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ConsolePrintTB = new System.Windows.Forms.RichTextBox();
            this.ValueFromWriingTextBox = new System.Windows.Forms.TextBox();
            this.valueFromReadingTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.valueToWriteTB = new System.Windows.Forms.TextBox();
            this.writeAddressTB = new System.Windows.Forms.TextBox();
            this.readAdressTB = new System.Windows.Forms.TextBox();
            this.valueFromWriting = new System.Windows.Forms.Label();
            this.valueFromReading = new System.Windows.Forms.Label();
            this.write_general_btn = new System.Windows.Forms.Button();
            this.read_general_btn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.enabledchaninstb = new System.Windows.Forms.TextBox();
            this.enabledchaninslbl = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.saveConfigToExcel = new System.Windows.Forms.Button();
            this.browseBtn = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxExcelpath = new System.Windows.Forms.TextBox();
            this.AGCaverageshiftyb = new System.Windows.Forms.TextBox();
            this.AGCaverageshiftlbl = new System.Windows.Forms.Label();
            this.AGCBACKOFFtb = new System.Windows.Forms.TextBox();
            this.AGCBACKOFFlbl = new System.Windows.Forms.Label();
            this.UL_Sub_Channel_0_Bandwidthtb = new System.Windows.Forms.TextBox();
            this.UL_Sub_Channel_0_Bandwidthlbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Transmit_zeros_from_BFtb = new System.Windows.Forms.TextBox();
            this.Transmit_zeros_from_BFlbl = new System.Windows.Forms.Label();
            this.Local_Entb = new System.Windows.Forms.TextBox();
            this.Local_Enlbl = new System.Windows.Forms.Label();
            this.Sub_Channel_0UL_CenterFrequencyOffsettb = new System.Windows.Forms.TextBox();
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl = new System.Windows.Forms.Label();
            this.ULFDDCenterFrequencytb = new System.Windows.Forms.TextBox();
            this.ULFDDCenterFrequencylbl = new System.Windows.Forms.Label();
            this.Sub_Channel_0DL_CenterFrequencyOffsettb = new System.Windows.Forms.TextBox();
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl = new System.Windows.Forms.Label();
            this.DLFDDCenterFrequencytb = new System.Windows.Forms.TextBox();
            this.DLFDDCenterFrequencylbl = new System.Windows.Forms.Label();
            this.DL_Sub_Channel_0_Bandwidthtb = new System.Windows.Forms.TextBox();
            this.DL_Sub_Channel_0lbl = new System.Windows.Forms.Label();
            this.PCS_UL_In_DL_Out_2tb = new System.Windows.Forms.TextBox();
            this.PCS_UL_In_DL_Out_2lbl = new System.Windows.Forms.Label();
            this.PCS_UL_In_DL_Out_1tb = new System.Windows.Forms.TextBox();
            this.PCS_UL_In_DL_Out_1lbl = new System.Windows.Forms.Label();
            this.PCS_DL_In_UL_Outtb = new System.Windows.Forms.TextBox();
            this.PCS_DL_In_UL_Outlbl = new System.Windows.Forms.Label();
            this.Micron_locationtb = new System.Windows.Forms.TextBox();
            this.Micron_locationlbl = new System.Windows.Forms.Label();
            this.Linkage_numbertb = new System.Windows.Forms.TextBox();
            this.Linkage_numberlbl = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.blocksPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.readAllBlocksBtn = new System.Windows.Forms.Button();
            this.TabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.AllowDrop = true;
            this.TabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl1.Controls.Add(this.tabPage1);
            this.TabControl1.Controls.Add(this.tabPage3);
            this.TabControl1.Controls.Add(this.tabPage2);
            this.TabControl1.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabControl1.Location = new System.Drawing.Point(8, 51);
            this.TabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(1423, 601);
            this.TabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.AllowDrop = true;
            this.tabPage1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage1.Controls.Add(this.terminalTextBox);
            this.tabPage1.Controls.Add(this.clearTerminalBtn);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(1415, 573);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Terminal ";
            // 
            // terminalTextBox
            // 
            this.terminalTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.terminalTextBox.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.terminalTextBox.Location = new System.Drawing.Point(7, 161);
            this.terminalTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.terminalTextBox.Name = "terminalTextBox";
            this.terminalTextBox.Size = new System.Drawing.Size(1403, 413);
            this.terminalTextBox.TabIndex = 94;
            this.terminalTextBox.Text = "";
            // 
            // clearTerminalBtn
            // 
            this.clearTerminalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.clearTerminalBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearTerminalBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.clearTerminalBtn.Location = new System.Drawing.Point(7, 131);
            this.clearTerminalBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.clearTerminalBtn.Name = "clearTerminalBtn";
            this.clearTerminalBtn.Size = new System.Drawing.Size(70, 27);
            this.clearTerminalBtn.TabIndex = 93;
            this.clearTerminalBtn.Text = "Clear";
            this.clearTerminalBtn.UseVisualStyleBackColor = false;
            this.clearTerminalBtn.Click += new System.EventHandler(this.clearTerminalBtn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.readAllBlocksBtn);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.NtoStartModetb);
            this.panel2.Controls.Add(this.StartModeBtn);
            this.panel2.Controls.Add(this.resetFPGAbtn);
            this.panel2.Controls.Add(this.resetSPIbtn);
            this.panel2.Controls.Add(this.sendFileBtn);
            this.panel2.Controls.Add(this.sendTextBtn);
            this.panel2.Controls.Add(this.sendFileTextBox);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.LoadTextFileBtn);
            this.panel2.Controls.Add(this.sendTextTextBox);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.OpenClosePortBtn);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.BitsPerSecondsComboBox);
            this.panel2.Controls.Add(this.ComComboBox);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1411, 127);
            this.panel2.TabIndex = 92;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(449, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 95;
            this.label1.Text = "SPI- N:";
            // 
            // NtoStartModetb
            // 
            this.NtoStartModetb.Location = new System.Drawing.Point(492, 10);
            this.NtoStartModetb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.NtoStartModetb.Name = "NtoStartModetb";
            this.NtoStartModetb.Size = new System.Drawing.Size(31, 21);
            this.NtoStartModetb.TabIndex = 94;
            this.NtoStartModetb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // StartModeBtn
            // 
            this.StartModeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.StartModeBtn.Font = new System.Drawing.Font("Candara", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartModeBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.StartModeBtn.Location = new System.Drawing.Point(445, 33);
            this.StartModeBtn.Name = "StartModeBtn";
            this.StartModeBtn.Size = new System.Drawing.Size(107, 29);
            this.StartModeBtn.TabIndex = 93;
            this.StartModeBtn.Text = "Start Mode";
            this.StartModeBtn.UseVisualStyleBackColor = false;
            this.StartModeBtn.Click += new System.EventHandler(this.StartModeBtn_Click);
            // 
            // resetFPGAbtn
            // 
            this.resetFPGAbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.resetFPGAbtn.Font = new System.Drawing.Font("Candara", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetFPGAbtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.resetFPGAbtn.Location = new System.Drawing.Point(313, 3);
            this.resetFPGAbtn.Name = "resetFPGAbtn";
            this.resetFPGAbtn.Size = new System.Drawing.Size(98, 29);
            this.resetFPGAbtn.TabIndex = 91;
            this.resetFPGAbtn.Text = "Reset FPGA";
            this.resetFPGAbtn.UseVisualStyleBackColor = false;
            this.resetFPGAbtn.Click += new System.EventHandler(this.resetFPGAbtn_Click);
            // 
            // resetSPIbtn
            // 
            this.resetSPIbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.resetSPIbtn.Font = new System.Drawing.Font("Candara", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetSPIbtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.resetSPIbtn.Location = new System.Drawing.Point(313, 33);
            this.resetSPIbtn.Name = "resetSPIbtn";
            this.resetSPIbtn.Size = new System.Drawing.Size(98, 29);
            this.resetSPIbtn.TabIndex = 92;
            this.resetSPIbtn.Text = "Reset SPI";
            this.resetSPIbtn.UseVisualStyleBackColor = false;
            this.resetSPIbtn.Click += new System.EventHandler(this.resetSPIbtn_Click);
            // 
            // sendFileBtn
            // 
            this.sendFileBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.sendFileBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendFileBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sendFileBtn.Location = new System.Drawing.Point(765, 92);
            this.sendFileBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sendFileBtn.Name = "sendFileBtn";
            this.sendFileBtn.Size = new System.Drawing.Size(59, 29);
            this.sendFileBtn.TabIndex = 90;
            this.sendFileBtn.Text = "Send";
            this.sendFileBtn.UseVisualStyleBackColor = false;
            this.sendFileBtn.Click += new System.EventHandler(this.sendFileBtn_Click);
            // 
            // sendTextBtn
            // 
            this.sendTextBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.sendTextBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendTextBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sendTextBtn.Location = new System.Drawing.Point(735, 63);
            this.sendTextBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sendTextBtn.Name = "sendTextBtn";
            this.sendTextBtn.Size = new System.Drawing.Size(88, 29);
            this.sendTextBtn.TabIndex = 89;
            this.sendTextBtn.Text = "Send";
            this.sendTextBtn.UseVisualStyleBackColor = false;
            this.sendTextBtn.Click += new System.EventHandler(this.sendTextBtn_Click);
            // 
            // sendFileTextBox
            // 
            this.sendFileTextBox.AllowDrop = true;
            this.sendFileTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.sendFileTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.sendFileTextBox.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendFileTextBox.Location = new System.Drawing.Point(82, 98);
            this.sendFileTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sendFileTextBox.Name = "sendFileTextBox";
            this.sendFileTextBox.Size = new System.Drawing.Size(651, 24);
            this.sendFileTextBox.TabIndex = 88;
            this.sendFileTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sendFileTextBox_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(4, 101);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 19);
            this.label13.TabIndex = 87;
            this.label13.Text = "Send File:";
            // 
            // LoadTextFileBtn
            // 
            this.LoadTextFileBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.LoadTextFileBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadTextFileBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LoadTextFileBtn.Location = new System.Drawing.Point(736, 92);
            this.LoadTextFileBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LoadTextFileBtn.Name = "LoadTextFileBtn";
            this.LoadTextFileBtn.Size = new System.Drawing.Size(28, 29);
            this.LoadTextFileBtn.TabIndex = 84;
            this.LoadTextFileBtn.Text = "...";
            this.LoadTextFileBtn.UseVisualStyleBackColor = false;
            this.LoadTextFileBtn.Click += new System.EventHandler(this.LoadTextFileBtn_Click);
            // 
            // sendTextTextBox
            // 
            this.sendTextTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.sendTextTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.sendTextTextBox.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendTextTextBox.Location = new System.Drawing.Point(82, 68);
            this.sendTextTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sendTextTextBox.Name = "sendTextTextBox";
            this.sendTextTextBox.Size = new System.Drawing.Size(651, 24);
            this.sendTextTextBox.TabIndex = 83;
            this.sendTextTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sendTextTextBox_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 70);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 19);
            this.label12.TabIndex = 82;
            this.label12.Text = "Send Text:";
            // 
            // OpenClosePortBtn
            // 
            this.OpenClosePortBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.OpenClosePortBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenClosePortBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.OpenClosePortBtn.Location = new System.Drawing.Point(212, 5);
            this.OpenClosePortBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.OpenClosePortBtn.Name = "OpenClosePortBtn";
            this.OpenClosePortBtn.Size = new System.Drawing.Size(65, 51);
            this.OpenClosePortBtn.TabIndex = 81;
            this.OpenClosePortBtn.Text = "Open";
            this.OpenClosePortBtn.UseVisualStyleBackColor = false;
            this.OpenClosePortBtn.Click += new System.EventHandler(this.OpenClosePortBtn_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(194, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(14, 49);
            this.panel1.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 39);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 15);
            this.label11.TabIndex = 3;
            this.label11.Text = "Bits Per Second";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 10);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 15);
            this.label10.TabIndex = 2;
            this.label10.Text = "COM Port:";
            // 
            // BitsPerSecondsComboBox
            // 
            this.BitsPerSecondsComboBox.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BitsPerSecondsComboBox.FormattingEnabled = true;
            this.BitsPerSecondsComboBox.Items.AddRange(new object[] {
            "115200",
            "230400",
            "460800"});
            this.BitsPerSecondsComboBox.Location = new System.Drawing.Point(95, 34);
            this.BitsPerSecondsComboBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BitsPerSecondsComboBox.Name = "BitsPerSecondsComboBox";
            this.BitsPerSecondsComboBox.Size = new System.Drawing.Size(94, 24);
            this.BitsPerSecondsComboBox.TabIndex = 1;
            // 
            // ComComboBox
            // 
            this.ComComboBox.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComComboBox.FormattingEnabled = true;
            this.ComComboBox.Location = new System.Drawing.Point(95, 6);
            this.ComComboBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComComboBox.Name = "ComComboBox";
            this.ComComboBox.Size = new System.Drawing.Size(94, 24);
            this.ComComboBox.TabIndex = 0;
            this.ComComboBox.Click += new System.EventHandler(this.ComComboBox_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage3.Controls.Add(this.ConsolePrintTB);
            this.tabPage3.Controls.Add(this.ValueFromWriingTextBox);
            this.tabPage3.Controls.Add(this.valueFromReadingTextBox);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.valueToWriteTB);
            this.tabPage3.Controls.Add(this.writeAddressTB);
            this.tabPage3.Controls.Add(this.readAdressTB);
            this.tabPage3.Controls.Add(this.valueFromWriting);
            this.tabPage3.Controls.Add(this.valueFromReading);
            this.tabPage3.Controls.Add(this.write_general_btn);
            this.tabPage3.Controls.Add(this.read_general_btn);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.enabledchaninstb);
            this.tabPage3.Controls.Add(this.enabledchaninslbl);
            this.tabPage3.Controls.Add(this.clearBtn);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.saveConfigToExcel);
            this.tabPage3.Controls.Add(this.browseBtn);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.textBoxExcelpath);
            this.tabPage3.Controls.Add(this.AGCaverageshiftyb);
            this.tabPage3.Controls.Add(this.AGCaverageshiftlbl);
            this.tabPage3.Controls.Add(this.AGCBACKOFFtb);
            this.tabPage3.Controls.Add(this.AGCBACKOFFlbl);
            this.tabPage3.Controls.Add(this.UL_Sub_Channel_0_Bandwidthtb);
            this.tabPage3.Controls.Add(this.UL_Sub_Channel_0_Bandwidthlbl);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.Transmit_zeros_from_BFtb);
            this.tabPage3.Controls.Add(this.Transmit_zeros_from_BFlbl);
            this.tabPage3.Controls.Add(this.Local_Entb);
            this.tabPage3.Controls.Add(this.Local_Enlbl);
            this.tabPage3.Controls.Add(this.Sub_Channel_0UL_CenterFrequencyOffsettb);
            this.tabPage3.Controls.Add(this.Sub_Channel_0UL_CenterFrequencyOffsetlbl);
            this.tabPage3.Controls.Add(this.ULFDDCenterFrequencytb);
            this.tabPage3.Controls.Add(this.ULFDDCenterFrequencylbl);
            this.tabPage3.Controls.Add(this.Sub_Channel_0DL_CenterFrequencyOffsettb);
            this.tabPage3.Controls.Add(this.Sub_Channel_0DL_CenterFrequencyOffsetlbl);
            this.tabPage3.Controls.Add(this.DLFDDCenterFrequencytb);
            this.tabPage3.Controls.Add(this.DLFDDCenterFrequencylbl);
            this.tabPage3.Controls.Add(this.DL_Sub_Channel_0_Bandwidthtb);
            this.tabPage3.Controls.Add(this.DL_Sub_Channel_0lbl);
            this.tabPage3.Controls.Add(this.PCS_UL_In_DL_Out_2tb);
            this.tabPage3.Controls.Add(this.PCS_UL_In_DL_Out_2lbl);
            this.tabPage3.Controls.Add(this.PCS_UL_In_DL_Out_1tb);
            this.tabPage3.Controls.Add(this.PCS_UL_In_DL_Out_1lbl);
            this.tabPage3.Controls.Add(this.PCS_DL_In_UL_Outtb);
            this.tabPage3.Controls.Add(this.PCS_DL_In_UL_Outlbl);
            this.tabPage3.Controls.Add(this.Micron_locationtb);
            this.tabPage3.Controls.Add(this.Micron_locationlbl);
            this.tabPage3.Controls.Add(this.Linkage_numbertb);
            this.tabPage3.Controls.Add(this.Linkage_numberlbl);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Size = new System.Drawing.Size(1415, 573);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "High Level Parameters ";
            // 
            // ConsolePrintTB
            // 
            this.ConsolePrintTB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConsolePrintTB.Location = new System.Drawing.Point(707, 45);
            this.ConsolePrintTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ConsolePrintTB.Name = "ConsolePrintTB";
            this.ConsolePrintTB.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.ConsolePrintTB.Size = new System.Drawing.Size(707, 533);
            this.ConsolePrintTB.TabIndex = 279;
            this.ConsolePrintTB.Text = "";
            // 
            // ValueFromWriingTextBox
            // 
            this.ValueFromWriingTextBox.Location = new System.Drawing.Point(483, 521);
            this.ValueFromWriingTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ValueFromWriingTextBox.Name = "ValueFromWriingTextBox";
            this.ValueFromWriingTextBox.Size = new System.Drawing.Size(75, 21);
            this.ValueFromWriingTextBox.TabIndex = 278;
            // 
            // valueFromReadingTextBox
            // 
            this.valueFromReadingTextBox.Location = new System.Drawing.Point(482, 491);
            this.valueFromReadingTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.valueFromReadingTextBox.Name = "valueFromReadingTextBox";
            this.valueFromReadingTextBox.Size = new System.Drawing.Size(75, 21);
            this.valueFromReadingTextBox.TabIndex = 277;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(207, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 22);
            this.label2.TabIndex = 224;
            this.label2.Text = "High Level Parameters";
            // 
            // valueToWriteTB
            // 
            this.valueToWriteTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.valueToWriteTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.valueToWriteTB.Location = new System.Drawing.Point(287, 523);
            this.valueToWriteTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.valueToWriteTB.Name = "valueToWriteTB";
            this.valueToWriteTB.Size = new System.Drawing.Size(66, 21);
            this.valueToWriteTB.TabIndex = 276;
            // 
            // writeAddressTB
            // 
            this.writeAddressTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.writeAddressTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.writeAddressTB.Location = new System.Drawing.Point(193, 523);
            this.writeAddressTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.writeAddressTB.Name = "writeAddressTB";
            this.writeAddressTB.Size = new System.Drawing.Size(79, 21);
            this.writeAddressTB.TabIndex = 275;
            // 
            // readAdressTB
            // 
            this.readAdressTB.AllowDrop = true;
            this.readAdressTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.readAdressTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.readAdressTB.Location = new System.Drawing.Point(193, 493);
            this.readAdressTB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.readAdressTB.Name = "readAdressTB";
            this.readAdressTB.Size = new System.Drawing.Size(79, 21);
            this.readAdressTB.TabIndex = 274;
            // 
            // valueFromWriting
            // 
            this.valueFromWriting.AutoSize = true;
            this.valueFromWriting.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueFromWriting.Location = new System.Drawing.Point(433, 525);
            this.valueFromWriting.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.valueFromWriting.Name = "valueFromWriting";
            this.valueFromWriting.Size = new System.Drawing.Size(42, 15);
            this.valueFromWriting.TabIndex = 273;
            this.valueFromWriting.Text = "Value:";
            // 
            // valueFromReading
            // 
            this.valueFromReading.AutoSize = true;
            this.valueFromReading.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueFromReading.Location = new System.Drawing.Point(433, 493);
            this.valueFromReading.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.valueFromReading.Name = "valueFromReading";
            this.valueFromReading.Size = new System.Drawing.Size(42, 15);
            this.valueFromReading.TabIndex = 272;
            this.valueFromReading.Text = "Value:";
            // 
            // write_general_btn
            // 
            this.write_general_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.write_general_btn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.write_general_btn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.write_general_btn.Location = new System.Drawing.Point(371, 517);
            this.write_general_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.write_general_btn.Name = "write_general_btn";
            this.write_general_btn.Size = new System.Drawing.Size(53, 25);
            this.write_general_btn.TabIndex = 271;
            this.write_general_btn.Text = "Write";
            this.write_general_btn.UseVisualStyleBackColor = false;
            this.write_general_btn.Click += new System.EventHandler(this.write_general_btn_Click);
            // 
            // read_general_btn
            // 
            this.read_general_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.read_general_btn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.read_general_btn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.read_general_btn.Location = new System.Drawing.Point(371, 487);
            this.read_general_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.read_general_btn.Name = "read_general_btn";
            this.read_general_btn.Size = new System.Drawing.Size(53, 25);
            this.read_general_btn.TabIndex = 270;
            this.read_general_btn.Text = "Read";
            this.read_general_btn.UseVisualStyleBackColor = false;
            this.read_general_btn.Click += new System.EventHandler(this.read_general_btn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(278, 471);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 15);
            this.label9.TabIndex = 269;
            this.label9.Text = "Value (To Write)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(208, 471);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 15);
            this.label8.TabIndex = 268;
            this.label8.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(110, 526);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 15);
            this.label7.TabIndex = 267;
            this.label7.Text = "Write Register";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(107, 493);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 15);
            this.label6.TabIndex = 266;
            this.label6.Text = "Read Register";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(407, 241);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 19);
            this.label5.TabIndex = 265;
            this.label5.Text = "UL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(91, 241);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 19);
            this.label4.TabIndex = 264;
            this.label4.Text = "DL";
            // 
            // enabledchaninstb
            // 
            this.enabledchaninstb.Location = new System.Drawing.Point(536, 199);
            this.enabledchaninstb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.enabledchaninstb.Name = "enabledchaninstb";
            this.enabledchaninstb.Size = new System.Drawing.Size(82, 21);
            this.enabledchaninstb.TabIndex = 263;
            // 
            // enabledchaninslbl
            // 
            this.enabledchaninslbl.AutoSize = true;
            this.enabledchaninslbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enabledchaninslbl.Location = new System.Drawing.Point(355, 199);
            this.enabledchaninslbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.enabledchaninslbl.Name = "enabledchaninslbl";
            this.enabledchaninslbl.Size = new System.Drawing.Size(123, 34);
            this.enabledchaninslbl.TabIndex = 262;
            this.enabledchaninslbl.Text = "Enabled chanins \r\n(antennas)";
            // 
            // clearBtn
            // 
            this.clearBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clearBtn.AutoSize = true;
            this.clearBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.clearBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.clearBtn.Location = new System.Drawing.Point(1348, 3);
            this.clearBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(65, 29);
            this.clearBtn.TabIndex = 261;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = false;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(949, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 23);
            this.label3.TabIndex = 260;
            this.label3.Text = "Console Prints";
            // 
            // saveConfigToExcel
            // 
            this.saveConfigToExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.saveConfigToExcel.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveConfigToExcel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.saveConfigToExcel.Location = new System.Drawing.Point(192, 421);
            this.saveConfigToExcel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.saveConfigToExcel.Name = "saveConfigToExcel";
            this.saveConfigToExcel.Size = new System.Drawing.Size(155, 36);
            this.saveConfigToExcel.TabIndex = 259;
            this.saveConfigToExcel.Text = "Save Configuration";
            this.saveConfigToExcel.UseVisualStyleBackColor = false;
            this.saveConfigToExcel.Click += new System.EventHandler(this.saveConfigToExcel_Click);
            // 
            // browseBtn
            // 
            this.browseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.browseBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.browseBtn.Location = new System.Drawing.Point(552, 39);
            this.browseBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.browseBtn.Name = "browseBtn";
            this.browseBtn.Size = new System.Drawing.Size(65, 29);
            this.browseBtn.TabIndex = 258;
            this.browseBtn.Text = "Browse";
            this.browseBtn.UseVisualStyleBackColor = false;
            this.browseBtn.Click += new System.EventHandler(this.browseBtn_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 25);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 19);
            this.label14.TabIndex = 257;
            this.label14.Text = "Excel File Path :";
            // 
            // textBoxExcelpath
            // 
            this.textBoxExcelpath.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxExcelpath.Location = new System.Drawing.Point(23, 44);
            this.textBoxExcelpath.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxExcelpath.Name = "textBoxExcelpath";
            this.textBoxExcelpath.Size = new System.Drawing.Size(527, 24);
            this.textBoxExcelpath.TabIndex = 256;
            // 
            // AGCaverageshiftyb
            // 
            this.AGCaverageshiftyb.Location = new System.Drawing.Point(536, 166);
            this.AGCaverageshiftyb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.AGCaverageshiftyb.Name = "AGCaverageshiftyb";
            this.AGCaverageshiftyb.Size = new System.Drawing.Size(82, 21);
            this.AGCaverageshiftyb.TabIndex = 255;
            // 
            // AGCaverageshiftlbl
            // 
            this.AGCaverageshiftlbl.AutoSize = true;
            this.AGCaverageshiftlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AGCaverageshiftlbl.Location = new System.Drawing.Point(355, 166);
            this.AGCaverageshiftlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.AGCaverageshiftlbl.Name = "AGCaverageshiftlbl";
            this.AGCaverageshiftlbl.Size = new System.Drawing.Size(131, 17);
            this.AGCaverageshiftlbl.TabIndex = 254;
            this.AGCaverageshiftlbl.Text = "AGC average shift";
            // 
            // AGCBACKOFFtb
            // 
            this.AGCBACKOFFtb.Location = new System.Drawing.Point(536, 135);
            this.AGCBACKOFFtb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.AGCBACKOFFtb.Name = "AGCBACKOFFtb";
            this.AGCBACKOFFtb.Size = new System.Drawing.Size(82, 21);
            this.AGCBACKOFFtb.TabIndex = 253;
            // 
            // AGCBACKOFFlbl
            // 
            this.AGCBACKOFFlbl.AutoSize = true;
            this.AGCBACKOFFlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AGCBACKOFFlbl.Location = new System.Drawing.Point(355, 135);
            this.AGCBACKOFFlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.AGCBACKOFFlbl.Name = "AGCBACKOFFlbl";
            this.AGCBACKOFFlbl.Size = new System.Drawing.Size(109, 17);
            this.AGCBACKOFFlbl.TabIndex = 252;
            this.AGCBACKOFFlbl.Text = "AGC BACKOFF";
            // 
            // UL_Sub_Channel_0_Bandwidthtb
            // 
            this.UL_Sub_Channel_0_Bandwidthtb.Location = new System.Drawing.Point(537, 283);
            this.UL_Sub_Channel_0_Bandwidthtb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.UL_Sub_Channel_0_Bandwidthtb.Name = "UL_Sub_Channel_0_Bandwidthtb";
            this.UL_Sub_Channel_0_Bandwidthtb.Size = new System.Drawing.Size(82, 21);
            this.UL_Sub_Channel_0_Bandwidthtb.TabIndex = 251;
            // 
            // UL_Sub_Channel_0_Bandwidthlbl
            // 
            this.UL_Sub_Channel_0_Bandwidthlbl.AutoSize = true;
            this.UL_Sub_Channel_0_Bandwidthlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UL_Sub_Channel_0_Bandwidthlbl.Location = new System.Drawing.Point(357, 270);
            this.UL_Sub_Channel_0_Bandwidthlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.UL_Sub_Channel_0_Bandwidthlbl.Name = "UL_Sub_Channel_0_Bandwidthlbl";
            this.UL_Sub_Channel_0_Bandwidthlbl.Size = new System.Drawing.Size(120, 34);
            this.UL_Sub_Channel_0_Bandwidthlbl.TabIndex = 250;
            this.UL_Sub_Channel_0_Bandwidthlbl.Text = "UL SubChannel 0\r\nBandwidth";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.button1.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(351, 421);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 36);
            this.button1.TabIndex = 249;
            this.button1.Text = "Excecute";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Transmit_zeros_from_BFtb
            // 
            this.Transmit_zeros_from_BFtb.Location = new System.Drawing.Point(537, 107);
            this.Transmit_zeros_from_BFtb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Transmit_zeros_from_BFtb.Name = "Transmit_zeros_from_BFtb";
            this.Transmit_zeros_from_BFtb.Size = new System.Drawing.Size(82, 21);
            this.Transmit_zeros_from_BFtb.TabIndex = 248;
            // 
            // Transmit_zeros_from_BFlbl
            // 
            this.Transmit_zeros_from_BFlbl.AutoSize = true;
            this.Transmit_zeros_from_BFlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Transmit_zeros_from_BFlbl.Location = new System.Drawing.Point(357, 107);
            this.Transmit_zeros_from_BFlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Transmit_zeros_from_BFlbl.Name = "Transmit_zeros_from_BFlbl";
            this.Transmit_zeros_from_BFlbl.Size = new System.Drawing.Size(155, 17);
            this.Transmit_zeros_from_BFlbl.TabIndex = 247;
            this.Transmit_zeros_from_BFlbl.Text = "Transmit zeros from BF";
            // 
            // Local_Entb
            // 
            this.Local_Entb.Location = new System.Drawing.Point(537, 77);
            this.Local_Entb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Local_Entb.Name = "Local_Entb";
            this.Local_Entb.Size = new System.Drawing.Size(82, 21);
            this.Local_Entb.TabIndex = 246;
            // 
            // Local_Enlbl
            // 
            this.Local_Enlbl.AutoSize = true;
            this.Local_Enlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Local_Enlbl.Location = new System.Drawing.Point(357, 78);
            this.Local_Enlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Local_Enlbl.Name = "Local_Enlbl";
            this.Local_Enlbl.Size = new System.Drawing.Size(64, 17);
            this.Local_Enlbl.TabIndex = 245;
            this.Local_Enlbl.Text = "Local En";
            // 
            // Sub_Channel_0UL_CenterFrequencyOffsettb
            // 
            this.Sub_Channel_0UL_CenterFrequencyOffsettb.Location = new System.Drawing.Point(539, 367);
            this.Sub_Channel_0UL_CenterFrequencyOffsettb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Sub_Channel_0UL_CenterFrequencyOffsettb.Name = "Sub_Channel_0UL_CenterFrequencyOffsettb";
            this.Sub_Channel_0UL_CenterFrequencyOffsettb.Size = new System.Drawing.Size(82, 21);
            this.Sub_Channel_0UL_CenterFrequencyOffsettb.TabIndex = 244;
            // 
            // Sub_Channel_0UL_CenterFrequencyOffsetlbl
            // 
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.AutoSize = true;
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.Location = new System.Drawing.Point(358, 361);
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.Name = "Sub_Channel_0UL_CenterFrequencyOffsetlbl";
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.Size = new System.Drawing.Size(127, 51);
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.TabIndex = 243;
            this.Sub_Channel_0UL_CenterFrequencyOffsetlbl.Text = "SubChannel 0 UL \r\nCenter Frequency\r\nOffset";
            // 
            // ULFDDCenterFrequencytb
            // 
            this.ULFDDCenterFrequencytb.Location = new System.Drawing.Point(537, 320);
            this.ULFDDCenterFrequencytb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ULFDDCenterFrequencytb.Name = "ULFDDCenterFrequencytb";
            this.ULFDDCenterFrequencytb.Size = new System.Drawing.Size(82, 21);
            this.ULFDDCenterFrequencytb.TabIndex = 242;
            // 
            // ULFDDCenterFrequencylbl
            // 
            this.ULFDDCenterFrequencylbl.AutoSize = true;
            this.ULFDDCenterFrequencylbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ULFDDCenterFrequencylbl.Location = new System.Drawing.Point(357, 315);
            this.ULFDDCenterFrequencylbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ULFDDCenterFrequencylbl.Name = "ULFDDCenterFrequencylbl";
            this.ULFDDCenterFrequencylbl.Size = new System.Drawing.Size(103, 34);
            this.ULFDDCenterFrequencylbl.TabIndex = 241;
            this.ULFDDCenterFrequencylbl.Text = "UL FDD Center\r\nFrequency";
            // 
            // Sub_Channel_0DL_CenterFrequencyOffsettb
            // 
            this.Sub_Channel_0DL_CenterFrequencyOffsettb.Location = new System.Drawing.Point(173, 361);
            this.Sub_Channel_0DL_CenterFrequencyOffsettb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Sub_Channel_0DL_CenterFrequencyOffsettb.Name = "Sub_Channel_0DL_CenterFrequencyOffsettb";
            this.Sub_Channel_0DL_CenterFrequencyOffsettb.Size = new System.Drawing.Size(82, 21);
            this.Sub_Channel_0DL_CenterFrequencyOffsettb.TabIndex = 240;
            // 
            // Sub_Channel_0DL_CenterFrequencyOffsetlbl
            // 
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.AutoSize = true;
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.Location = new System.Drawing.Point(25, 350);
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.Name = "Sub_Channel_0DL_CenterFrequencyOffsetlbl";
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.Size = new System.Drawing.Size(127, 51);
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.TabIndex = 239;
            this.Sub_Channel_0DL_CenterFrequencyOffsetlbl.Text = "SubChannel 0 DL \r\nCenter Frequency\r\nOffset";
            // 
            // DLFDDCenterFrequencytb
            // 
            this.DLFDDCenterFrequencytb.Location = new System.Drawing.Point(172, 318);
            this.DLFDDCenterFrequencytb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DLFDDCenterFrequencytb.Name = "DLFDDCenterFrequencytb";
            this.DLFDDCenterFrequencytb.Size = new System.Drawing.Size(82, 21);
            this.DLFDDCenterFrequencytb.TabIndex = 238;
            // 
            // DLFDDCenterFrequencylbl
            // 
            this.DLFDDCenterFrequencylbl.AutoSize = true;
            this.DLFDDCenterFrequencylbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DLFDDCenterFrequencylbl.Location = new System.Drawing.Point(24, 311);
            this.DLFDDCenterFrequencylbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.DLFDDCenterFrequencylbl.Name = "DLFDDCenterFrequencylbl";
            this.DLFDDCenterFrequencylbl.Size = new System.Drawing.Size(104, 34);
            this.DLFDDCenterFrequencylbl.TabIndex = 237;
            this.DLFDDCenterFrequencylbl.Text = "DL FDD Center\r\nFrequency";
            // 
            // DL_Sub_Channel_0_Bandwidthtb
            // 
            this.DL_Sub_Channel_0_Bandwidthtb.Location = new System.Drawing.Point(173, 279);
            this.DL_Sub_Channel_0_Bandwidthtb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DL_Sub_Channel_0_Bandwidthtb.Name = "DL_Sub_Channel_0_Bandwidthtb";
            this.DL_Sub_Channel_0_Bandwidthtb.Size = new System.Drawing.Size(82, 21);
            this.DL_Sub_Channel_0_Bandwidthtb.TabIndex = 236;
            // 
            // DL_Sub_Channel_0lbl
            // 
            this.DL_Sub_Channel_0lbl.AutoSize = true;
            this.DL_Sub_Channel_0lbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DL_Sub_Channel_0lbl.Location = new System.Drawing.Point(25, 270);
            this.DL_Sub_Channel_0lbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.DL_Sub_Channel_0lbl.Name = "DL_Sub_Channel_0lbl";
            this.DL_Sub_Channel_0lbl.Size = new System.Drawing.Size(121, 34);
            this.DL_Sub_Channel_0lbl.TabIndex = 235;
            this.DL_Sub_Channel_0lbl.Text = "DL SubChannel 0\r\nBandwidth";
            // 
            // PCS_UL_In_DL_Out_2tb
            // 
            this.PCS_UL_In_DL_Out_2tb.Location = new System.Drawing.Point(173, 197);
            this.PCS_UL_In_DL_Out_2tb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PCS_UL_In_DL_Out_2tb.Name = "PCS_UL_In_DL_Out_2tb";
            this.PCS_UL_In_DL_Out_2tb.Size = new System.Drawing.Size(82, 21);
            this.PCS_UL_In_DL_Out_2tb.TabIndex = 234;
            // 
            // PCS_UL_In_DL_Out_2lbl
            // 
            this.PCS_UL_In_DL_Out_2lbl.AutoSize = true;
            this.PCS_UL_In_DL_Out_2lbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PCS_UL_In_DL_Out_2lbl.Location = new System.Drawing.Point(25, 197);
            this.PCS_UL_In_DL_Out_2lbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PCS_UL_In_DL_Out_2lbl.Name = "PCS_UL_In_DL_Out_2lbl";
            this.PCS_UL_In_DL_Out_2lbl.Size = new System.Drawing.Size(129, 17);
            this.PCS_UL_In_DL_Out_2lbl.TabIndex = 233;
            this.PCS_UL_In_DL_Out_2lbl.Text = "PCS UL In DL Out 2";
            // 
            // PCS_UL_In_DL_Out_1tb
            // 
            this.PCS_UL_In_DL_Out_1tb.Location = new System.Drawing.Point(173, 166);
            this.PCS_UL_In_DL_Out_1tb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PCS_UL_In_DL_Out_1tb.Name = "PCS_UL_In_DL_Out_1tb";
            this.PCS_UL_In_DL_Out_1tb.Size = new System.Drawing.Size(82, 21);
            this.PCS_UL_In_DL_Out_1tb.TabIndex = 232;
            // 
            // PCS_UL_In_DL_Out_1lbl
            // 
            this.PCS_UL_In_DL_Out_1lbl.AutoSize = true;
            this.PCS_UL_In_DL_Out_1lbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PCS_UL_In_DL_Out_1lbl.Location = new System.Drawing.Point(25, 166);
            this.PCS_UL_In_DL_Out_1lbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PCS_UL_In_DL_Out_1lbl.Name = "PCS_UL_In_DL_Out_1lbl";
            this.PCS_UL_In_DL_Out_1lbl.Size = new System.Drawing.Size(133, 17);
            this.PCS_UL_In_DL_Out_1lbl.TabIndex = 231;
            this.PCS_UL_In_DL_Out_1lbl.Text = "PCS UL In DL Out 1 ";
            // 
            // PCS_DL_In_UL_Outtb
            // 
            this.PCS_DL_In_UL_Outtb.Location = new System.Drawing.Point(173, 136);
            this.PCS_DL_In_UL_Outtb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PCS_DL_In_UL_Outtb.Name = "PCS_DL_In_UL_Outtb";
            this.PCS_DL_In_UL_Outtb.Size = new System.Drawing.Size(82, 21);
            this.PCS_DL_In_UL_Outtb.TabIndex = 230;
            // 
            // PCS_DL_In_UL_Outlbl
            // 
            this.PCS_DL_In_UL_Outlbl.AutoSize = true;
            this.PCS_DL_In_UL_Outlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PCS_DL_In_UL_Outlbl.Location = new System.Drawing.Point(25, 136);
            this.PCS_DL_In_UL_Outlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PCS_DL_In_UL_Outlbl.Name = "PCS_DL_In_UL_Outlbl";
            this.PCS_DL_In_UL_Outlbl.Size = new System.Drawing.Size(121, 17);
            this.PCS_DL_In_UL_Outlbl.TabIndex = 229;
            this.PCS_DL_In_UL_Outlbl.Text = "PCS DL In UL Out ";
            // 
            // Micron_locationtb
            // 
            this.Micron_locationtb.Location = new System.Drawing.Point(173, 108);
            this.Micron_locationtb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Micron_locationtb.Name = "Micron_locationtb";
            this.Micron_locationtb.Size = new System.Drawing.Size(82, 21);
            this.Micron_locationtb.TabIndex = 228;
            // 
            // Micron_locationlbl
            // 
            this.Micron_locationlbl.AutoSize = true;
            this.Micron_locationlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Micron_locationlbl.Location = new System.Drawing.Point(25, 108);
            this.Micron_locationlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Micron_locationlbl.Name = "Micron_locationlbl";
            this.Micron_locationlbl.Size = new System.Drawing.Size(121, 17);
            this.Micron_locationlbl.TabIndex = 227;
            this.Micron_locationlbl.Text = "Micron Location ";
            // 
            // Linkage_numbertb
            // 
            this.Linkage_numbertb.Location = new System.Drawing.Point(173, 79);
            this.Linkage_numbertb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Linkage_numbertb.Name = "Linkage_numbertb";
            this.Linkage_numbertb.Size = new System.Drawing.Size(82, 21);
            this.Linkage_numbertb.TabIndex = 226;
            // 
            // Linkage_numberlbl
            // 
            this.Linkage_numberlbl.AutoSize = true;
            this.Linkage_numberlbl.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Linkage_numberlbl.Location = new System.Drawing.Point(25, 79);
            this.Linkage_numberlbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Linkage_numberlbl.Name = "Linkage_numberlbl";
            this.Linkage_numberlbl.Size = new System.Drawing.Size(119, 17);
            this.Linkage_numberlbl.TabIndex = 225;
            this.Linkage_numberlbl.Text = "Linkage Number";
            // 
            // tabPage2
            // 
            this.tabPage2.AllowDrop = true;
            this.tabPage2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage2.Controls.Add(this.buttonOpen);
            this.tabPage2.Controls.Add(this.blocksPanel);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Size = new System.Drawing.Size(1415, 573);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Blocks ";
            // 
            // buttonOpen
            // 
            this.buttonOpen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.buttonOpen.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonOpen.Location = new System.Drawing.Point(7, 304);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(111, 29);
            this.buttonOpen.TabIndex = 8;
            this.buttonOpen.Text = "Open";
            this.buttonOpen.UseVisualStyleBackColor = false;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // blocksPanel
            // 
            this.blocksPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blocksPanel.AutoScroll = true;
            this.blocksPanel.AutoSize = true;
            this.blocksPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.blocksPanel.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blocksPanel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.blocksPanel.Location = new System.Drawing.Point(8, 6);
            this.blocksPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.blocksPanel.Name = "blocksPanel";
            this.blocksPanel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.blocksPanel.Size = new System.Drawing.Size(880, 354);
            this.blocksPanel.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(136, 43);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // readAllBlocksBtn
            // 
            this.readAllBlocksBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.readAllBlocksBtn.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readAllBlocksBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.readAllBlocksBtn.Location = new System.Drawing.Point(578, 5);
            this.readAllBlocksBtn.Margin = new System.Windows.Forms.Padding(2);
            this.readAllBlocksBtn.Name = "readAllBlocksBtn";
            this.readAllBlocksBtn.Size = new System.Drawing.Size(90, 57);
            this.readAllBlocksBtn.TabIndex = 96;
            this.readAllBlocksBtn.Text = "Read All\r\nBlocks";
            this.readAllBlocksBtn.UseVisualStyleBackColor = false;
            this.readAllBlocksBtn.Click += new System.EventHandler(this.readAllBlocksBtn_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1283, 660);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.TabControl1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Main";
            this.Text = "FPGA mGUI";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.TabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.RichTextBox terminalTextBox;
        private System.Windows.Forms.Button clearTerminalBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button sendFileBtn;
        private System.Windows.Forms.Button sendTextBtn;
        private System.Windows.Forms.TextBox sendFileTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button LoadTextFileBtn;
        private System.Windows.Forms.TextBox sendTextTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button OpenClosePortBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox BitsPerSecondsComboBox;
        private System.Windows.Forms.ComboBox ComComboBox;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Panel blocksPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button resetFPGAbtn;
        private System.Windows.Forms.Button resetSPIbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NtoStartModetb;
        private System.Windows.Forms.Button StartModeBtn;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox ConsolePrintTB;
        private System.Windows.Forms.TextBox ValueFromWriingTextBox;
        private System.Windows.Forms.TextBox valueFromReadingTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox valueToWriteTB;
        private System.Windows.Forms.TextBox writeAddressTB;
        private System.Windows.Forms.TextBox readAdressTB;
        private System.Windows.Forms.Label valueFromWriting;
        private System.Windows.Forms.Label valueFromReading;
        private System.Windows.Forms.Button write_general_btn;
        private System.Windows.Forms.Button read_general_btn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox enabledchaninstb;
        private System.Windows.Forms.Label enabledchaninslbl;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button saveConfigToExcel;
        private System.Windows.Forms.Button browseBtn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxExcelpath;
        private System.Windows.Forms.TextBox AGCaverageshiftyb;
        private System.Windows.Forms.Label AGCaverageshiftlbl;
        private System.Windows.Forms.TextBox AGCBACKOFFtb;
        private System.Windows.Forms.Label AGCBACKOFFlbl;
        private System.Windows.Forms.TextBox UL_Sub_Channel_0_Bandwidthtb;
        private System.Windows.Forms.Label UL_Sub_Channel_0_Bandwidthlbl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Transmit_zeros_from_BFtb;
        private System.Windows.Forms.Label Transmit_zeros_from_BFlbl;
        private System.Windows.Forms.TextBox Local_Entb;
        private System.Windows.Forms.Label Local_Enlbl;
        private System.Windows.Forms.TextBox Sub_Channel_0UL_CenterFrequencyOffsettb;
        private System.Windows.Forms.Label Sub_Channel_0UL_CenterFrequencyOffsetlbl;
        private System.Windows.Forms.TextBox ULFDDCenterFrequencytb;
        private System.Windows.Forms.Label ULFDDCenterFrequencylbl;
        private System.Windows.Forms.TextBox Sub_Channel_0DL_CenterFrequencyOffsettb;
        private System.Windows.Forms.Label Sub_Channel_0DL_CenterFrequencyOffsetlbl;
        private System.Windows.Forms.TextBox DLFDDCenterFrequencytb;
        private System.Windows.Forms.Label DLFDDCenterFrequencylbl;
        private System.Windows.Forms.TextBox DL_Sub_Channel_0_Bandwidthtb;
        private System.Windows.Forms.Label DL_Sub_Channel_0lbl;
        private System.Windows.Forms.TextBox PCS_UL_In_DL_Out_2tb;
        private System.Windows.Forms.Label PCS_UL_In_DL_Out_2lbl;
        private System.Windows.Forms.TextBox PCS_UL_In_DL_Out_1tb;
        private System.Windows.Forms.Label PCS_UL_In_DL_Out_1lbl;
        private System.Windows.Forms.TextBox PCS_DL_In_UL_Outtb;
        private System.Windows.Forms.Label PCS_DL_In_UL_Outlbl;
        private System.Windows.Forms.TextBox Micron_locationtb;
        private System.Windows.Forms.Label Micron_locationlbl;
        private System.Windows.Forms.TextBox Linkage_numbertb;
        private System.Windows.Forms.Label Linkage_numberlbl;
        private System.Windows.Forms.Button readAllBlocksBtn;
    }
}

