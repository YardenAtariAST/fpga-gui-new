﻿namespace FPGA_mGUI
{
    partial class generalBlock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(generalBlock));
            this.block_name_lbl = new System.Windows.Forms.Label();
            this.readAllBtn = new System.Windows.Forms.Button();
            this.writeAllBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // block_name_lbl
            // 
            this.block_name_lbl.AutoSize = true;
            this.block_name_lbl.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.block_name_lbl.Location = new System.Drawing.Point(12, 11);
            this.block_name_lbl.Name = "block_name_lbl";
            this.block_name_lbl.Size = new System.Drawing.Size(224, 43);
            this.block_name_lbl.TabIndex = 0;
            this.block_name_lbl.Text = "BlockName";
            // 
            // readAllBtn
            // 
            this.readAllBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.readAllBtn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readAllBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.readAllBtn.Location = new System.Drawing.Point(243, 9);
            this.readAllBtn.Margin = new System.Windows.Forms.Padding(4);
            this.readAllBtn.Name = "readAllBtn";
            this.readAllBtn.Size = new System.Drawing.Size(167, 45);
            this.readAllBtn.TabIndex = 9;
            this.readAllBtn.Text = "Read All";
            this.readAllBtn.UseVisualStyleBackColor = false;
            this.readAllBtn.Click += new System.EventHandler(this.readAllBtn_Click);
            // 
            // writeAllBtn
            // 
            this.writeAllBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(43)))), ((int)(((byte)(79)))));
            this.writeAllBtn.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.writeAllBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.writeAllBtn.Location = new System.Drawing.Point(418, 9);
            this.writeAllBtn.Margin = new System.Windows.Forms.Padding(4);
            this.writeAllBtn.Name = "writeAllBtn";
            this.writeAllBtn.Size = new System.Drawing.Size(167, 45);
            this.writeAllBtn.TabIndex = 10;
            this.writeAllBtn.Text = "Write All";
            this.writeAllBtn.UseVisualStyleBackColor = false;
            this.writeAllBtn.Click += new System.EventHandler(this.writeAllBtn_Click);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel.Location = new System.Drawing.Point(13, 63);
            this.flowLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(1548, 805);
            this.flowLayoutPanel.TabIndex = 11;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(1352, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(209, 67);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // generalBlock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1574, 882);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.writeAllBtn);
            this.Controls.Add(this.readAllBtn);
            this.Controls.Add(this.block_name_lbl);
            this.Name = "generalBlock";
            this.Text = "generalBlock";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label block_name_lbl;
        private System.Windows.Forms.Button readAllBtn;
        private System.Windows.Forms.Button writeAllBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}