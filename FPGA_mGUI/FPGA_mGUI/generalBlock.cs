﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FPGA_mGUI.Classes;
using System.IO;
using System.IO.Ports;

namespace FPGA_mGUI
{
    public partial class generalBlock : Form
    {
        private SerialPort serialPort1;
        const string FLAG_TO_NOTHING = "No connection";
        Block block;

        public generalBlock(Block block1,ref SerialPort serialPort)
        {
            block = block1;
            serialPort1 = serialPort;
            if (!serialPort1.IsOpen)
                serialPort1.Open();

            InitializeComponent();

            block_name_lbl.Text = block.BlockName.ToString();
            createUIfromBlockMem(block);

        }

        private void createUIfromBlockMem(Block block)
        {
            int numOfReg = -1;
            foreach (Register register in block.RegistersList)
            {
                GroupBox groupBox1 = new GroupBox();
                groupBox1.Text = register.registerName + "\n" + "0x" + register.addressOffset.ToString("X");
                groupBox1.Name = register.registerName + "\n" + "0x" + register.addressOffset.ToString("X");
                numOfReg++;
                groupBox1.Tag = numOfReg;

                int ypoint = 8;
                if (register.registerName.Length > 19)
                    ypoint = 30;
               
                int num_of_fields_on_struct = 0;
                int last_field_location = 30;
                foreach (Field fieldMem in register.fieldsList)
                {
                    num_of_fields_on_struct++;
                    Label field = new Label();

                    if (num_of_fields_on_struct > 0)
                        ypoint += 22;

                    field.Location = new Point(2, ypoint);
                    field.Name = fieldMem.fieldName;
                    field.Size = new Size(157, 15);
                    field.Text = $"{fieldMem.fieldName}:{fieldMem.fieldBits} ({fieldMem.startIndex}-{fieldMem.endIndex})";

                    last_field_location = ypoint;

                    TextBox textBox = new TextBox();
                    ypoint += 17;
                    textBox.Location = new Point(8, ypoint);
                    textBox.Name = fieldMem.fieldName;
                    textBox.Size = new Size(80, 6);

                    groupBox1.Controls.Add(field);
                    groupBox1.Controls.Add(textBox);
                    groupBox1.Size = new Size(160, ypoint + 80);

                }

                CheckBox isbyValuecheckbox = new CheckBox();
                isbyValuecheckbox.Location = new Point(2, ypoint + 26);


                Label valuelbl = new Label();
                valuelbl.Text = "Value:";
                valuelbl.Location = new Point(15, ypoint + 30);
                valuelbl.Name = "value_label";
                valuelbl.Size = new Size(45, 15);

                TextBox textBoxvalue = new TextBox();
                textBoxvalue.Location = new Point(valuelbl.ClientSize.Width + 15, ypoint + 27);
                textBoxvalue.Name = "tbValue";
                textBoxvalue.BringToFront();
                textBoxvalue.Size = new Size(78, 10);

                Button read_btn = new Button();
                Button write_btn = new Button();
                read_btn.Location = new Point(8, ypoint + 50);
                read_btn.Name = "read";
                read_btn.BackColor = Color.SteelBlue;
                read_btn.ForeColor = Color.White;
                read_btn.Text = "Read";
                read_btn.FlatStyle = FlatStyle.Flat;
                read_btn.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
                read_btn.FlatAppearance.BorderSize = 0;
                read_btn.Size = new Size(68, 22);
                write_btn.Location = new Point(83, ypoint + 50);
                write_btn.Name = "write_btn";
                write_btn.Text = "Write";
                write_btn.BackColor = Color.SteelBlue;
                write_btn.FlatStyle = FlatStyle.Flat;
                write_btn.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
                write_btn.FlatAppearance.BorderSize = 0;
                write_btn.ForeColor = Color.White;
                write_btn.Size = new Size(68, 22);


                groupBox1.Controls.Add(textBoxvalue);
                groupBox1.Controls.Add(valuelbl);
                groupBox1.Controls.Add(isbyValuecheckbox);
                groupBox1.Controls.Add(read_btn);
                groupBox1.Controls.Add(write_btn);

                write_btn.Click += new EventHandler(write_btn_Click);
                read_btn.Click += new EventHandler(read_btn_Click);
                flowLayoutPanel.Controls.Add(groupBox1);
            }


        }

        //General

        public void setSerialPortFromTerminal(ref SerialPort serialPort)
        {
            serialPort1 = serialPort;
        }
        public string ReadFromPort(int index, string eot = "FC>")
        {
            string answerLineFromWrite = "";
            int timeOFtimeout = 0;
            Boolean flagToNothing = false;

            while (!answerLineFromWrite.Contains(eot))
            {
                try
                {
                    int numberOfBytes = serialPort1.BytesToRead;
                    if (numberOfBytes > 0)
                        answerLineFromWrite += serialPort1.ReadExisting();
                }
                catch (TimeoutException ex)
                {
                    timeOFtimeout++;

                    if (timeOFtimeout > 5)
                    {
                        flagToNothing = true;
                        break;
                    }
                    else
                        continue;
                }

            }
            if (flagToNothing) return FLAG_TO_NOTHING;
            else return answerLineFromWrite;
        }
        public string ParseReplyFromPort(string replyFromPort)
        {
            string[] arrayreplyFromPort = replyFromPort.Split('\n');
            int indexOfOkline = -1;
            string valueFromParse = "";
            for (int i = 0; i < arrayreplyFromPort.Length; i++)
            {
                if (arrayreplyFromPort[i].Contains("Value"))
                {
                    int indexOfEqual = arrayreplyFromPort[i].IndexOf("Value=");
                    valueFromParse = arrayreplyFromPort[i].Substring(indexOfEqual + 6).Trim();
                    break;
                }
            }
            return valueFromParse;
        }
        public List<string> getFieldValuesListFromValueFromParse(Register register, string valueFromParse)
        {
            List<Field> fieldsList = register.fieldsList;
            List<int> fieldsBitsList = new List<int>();
            List<string> fieldsValuesList = new List<string>();
            foreach (Field field in fieldsList)
            {
                fieldsBitsList.Add(field.fieldBits);
            }
            string binStringValue = HexStringToBinString(valueFromParse);
            int lastI = binStringValue.Length - 1;
            int indexToListBitsToRead = 0;
            for (int i = binStringValue.Length; i >-2 ;i--)
            {
                i = lastI;
                if (indexToListBitsToRead == fieldsBitsList.Count)
                    break;
                int bitsToreadInt = fieldsBitsList[indexToListBitsToRead];
                string bitsOfLabel = "";
                indexToListBitsToRead++;
                for (int j = 0; j < bitsToreadInt; j++)
                {
                    bitsOfLabel = binStringValue[i] + bitsOfLabel;
                    if (i == 0) break;
                    else i--;
                }
                i--;
                string fieldValueHex = BinStringToHexString(bitsOfLabel);
                fieldsValuesList.Add(fieldValueHex);
                lastI = i + 1;

            }


            return fieldsValuesList;
        }


        // Read & Write register
        private void read_btn_Click(object sender, System.EventArgs e)
        {
            Button clickedButton = sender as Button;
            GroupBox groupBox = (GroupBox)clickedButton.Parent;
            int index = (int)groupBox.Tag;
            string replyFromPort = "";
            string address = $"0x{block.RegistersList[index].addressOffset.ToString("X")}";
            try
            {
                if (!serialPort1.IsOpen)
                    serialPort1.Open();
                serialPort1.Write($"fpga readreg {address}\r\n");
                replyFromPort = ReadFromPort(index);

                string valueFromParse = ParseReplyFromPort(replyFromPort);
                Register register = block.RegistersList[index];
                if (valueFromParse != "")
                {
                    register.fullValue = valueFromParse;
                    List<string> fieldsValueList= getFieldValuesListFromValueFromParse(register, valueFromParse);
                    updatebitsFieldFromReading(groupBox, fieldsValueList, valueFromParse);
                }
                else
                {
                    MessageBox.Show("Reading Failed");
                }
            }
            catch (TimeoutException ex)
            {
                MessageBox.Show("Error:", ex.Message);
            }

        }
        private void write_btn_Click(object sender, System.EventArgs e)
        {
            if (!serialPort1.IsOpen)
                serialPort1.Open();
            Button clickedButton = sender as Button;
            GroupBox groupBox = (GroupBox)clickedButton.Parent;
            int index = (int)groupBox.Tag;
            var textBoxList = clickedButton.Parent.Controls.OfType<TextBox>();
            Boolean isByvalue = clickedButton.Parent.Controls.OfType<CheckBox>().First().Checked;
            Register register = block.RegistersList[index];

            string replyFromPort = "";
            string address = $"0x{block.RegistersList[index].addressOffset.ToString("X")}";
            string fullValue = "";
            if (isByvalue)
            {
                foreach (TextBox tb in textBoxList)
                {
                    if (tb.Name.Contains("Value"))
                    {
                        try
                        {
                            if (tb.Text.Contains("0x"))
                                fullValue = tb.Text.Substring(2).Trim();
                            else fullValue = tb.Text;
                            serialPort1.Write($"fpga writereg {address} {fullValue}\r\n");
                            replyFromPort = ReadFromPort(index);
                            
                            if (replyFromPort.Contains("OK"))
                            {
                                register.fullValue = fullValue;
                                List<string> fieldsValueList = getFieldValuesListFromValueFromParse(register, fullValue);
                                updatebitsFieldFromReading(groupBox, fieldsValueList, fullValue);
                            }
                            else
                            {
                                MessageBox.Show("Writing Failed");
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error:", ex.Message);
                        }
                        break;
                    }
                    else continue;
                }
            }
            // By bits
            else
            {
                List<string> fieldsValuesListFromUser = new List<string>();
                foreach (TextBox tb in textBoxList)
                {
                    if (tb.Name.Contains("Value")) continue;
                    else
                    {
                        fieldsValuesListFromUser.Add(tb.Text);
                    }
                }
                if (fieldsValuesListFromUser.Count != textBoxList.Count() - 1) MessageBox.Show("All fields are required. You can read before you write");
                else
                {
                    List<Field> fieldsList = register.fieldsList;
                    List<int> fieldsBitsList = new List<int>();
                    foreach (Field field in fieldsList)
                    {
                        fieldsBitsList.Add(field.fieldBits);
                    }
                    string fullBinString = "";
                    string fullValueString = "";
                    int i = 0;
                    Boolean flagToUnmatchedBits = false;
                    foreach (TextBox tb in textBoxList)
                    {
                        if (tb.Name.Contains("Value")) continue;
                        string binString = HexStringToBinStringWithoutPadding(tb.Text);


                        if (binString.Length > Convert.ToInt32(fieldsBitsList[i]))
                        {
                            MessageBox.Show($"Your input is bigger than the required field size -> {fieldsBitsList[i]} bits");
                            fullBinString = "";
                            flagToUnmatchedBits = true;
                            break;
                        }
                        else
                        {
                            if (binString.Length < Convert.ToInt32(fieldsBitsList[i]))
                            {
                                int numOfZeroToadd = (Convert.ToInt32(fieldsBitsList[i]) - binString.Length);
                                for (int j = 0; j < numOfZeroToadd; j++)
                                    binString = "0" + binString;
                            }
                            fullBinString = binString + fullBinString;
                            i++;
                            continue;
                        }
                        i++;
                    }
                    if (!flagToUnmatchedBits)
                    {
                        fullValueString = BinStringToHexString(fullBinString);
                        foreach (TextBox tb in textBoxList)
                        {
                            if (tb.Name.Contains("Value"))
                            {
                                updateFullValueFromReading(tb, "0x" + fullValueString);
                            }

                        }
                    }
                }

            }
        }
        private void readAllBtn_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
                serialPort1.Open();
            var groupBoxList= flowLayoutPanel.Controls.OfType<GroupBox>();
            var btnReadList = new List<Button>();

            foreach (GroupBox gb in groupBoxList)
            {
                var btInSpecificGBList = gb.Controls.OfType<Button>();
                foreach (Button btn in btInSpecificGBList)
                {
                    if (btn.Name.Contains("read"))
                    {
                        btn.PerformClick();
                    }

                }

            }
        }
        private void writeAllBtn_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
                serialPort1.Open();
            var groupBoxList = flowLayoutPanel.Controls.OfType<GroupBox>();
            var btnReadList = new List<Button>();

            try
            {
                foreach (GroupBox gb in groupBoxList)
                {
                    var btInSpecificGBList = gb.Controls.OfType<Button>();
                    foreach (Button btn in btInSpecificGBList)
                    {
                        if (btn.Name.Equals("write"))
                        {
                            btn.PerformClick();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error!");
            }
        }


        //UI changes
        public delegate void updateFullValueFromReadingCallback(TextBox valueTextBox, string valueFromParse);
        public void updateFullValueFromReading(TextBox valueTextBox, string valueFromParse)
        {
            if (valueTextBox.InvokeRequired)
            {
                updateFullValueFromReadingCallback call = new updateFullValueFromReadingCallback(updateFullValueFromReading);
                valueTextBox.BeginInvoke(call, valueTextBox, valueFromParse);
            }
            else
            {
                valueTextBox.Text = valueFromParse;
            }
        }

        public delegate void updatebitsFieldAndValueFromReadingCallback(GroupBox groupBox, List<string> fieldsValuesList, string valueFromParse);
        public void updatebitsFieldFromReading(GroupBox groupBox, List<string> fieldsValuesList, string valueFromParse)
        {
            if (groupBox.InvokeRequired)
            {
                updatebitsFieldAndValueFromReadingCallback call = new updatebitsFieldAndValueFromReadingCallback(updatebitsFieldFromReading);
                groupBox.BeginInvoke(call, groupBox, fieldsValuesList, valueFromParse);
            }
            else
            {
                var textBoxesList = groupBox.Controls.OfType<TextBox>();
                int indexFieldValue = 0;
                foreach (TextBox tb in textBoxesList)
                {
                    if (tb.Name.Contains("Value"))
                    {
                        tb.Text = valueFromParse;
                    }
                    else
                    {
                        if (indexFieldValue <= fieldsValuesList.Count-1)
                        {
                            tb.Text = fieldsValuesList[indexFieldValue];
                            indexFieldValue++;
                        }
                    }
                    
                }
            }
        }



        // Convertion Functions
        public static string HexStringToBinString(string hexString)
        {
            string binString = Convert.ToString(Convert.ToInt32(hexString, 16), 2).PadLeft(32, '0');
            return binString;
        }
        public static string BinStringToHexString(string binString)
        {
            string hexString = Convert.ToInt32(binString, 2).ToString("X");
            return hexString;
        }
        public static string HexStringToBinStringWithoutPadding(string hexString)
        {
            string binString = Convert.ToString(Convert.ToInt32(hexString, 16), 2);
            return binString;
        }

     
    }

}
