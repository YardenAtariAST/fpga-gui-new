﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;
using FPGA_mGUI.Classes;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace FPGA_mGUI
{
    public partial class Main : Form
    {
        private SerialPort serialPort1;
        Boolean isComConfig = false;
        const string EOM = "FC>";
        const string RESET_ID = "-----+";
        const string READ_REG = "fpga readreg ";
        const string WRITE_REG = "fpga writereg ";

        const string UserInputSendTextTXTfile = "autoCompleteSendText.txt";
        const string UserInputSendFileTXTfile = "autoCompleteSendFile.txt";
        static AutoCompleteUserInputsAddress autocompleteSendText = new AutoCompleteUserInputsAddress(UserInputSendTextTXTfile);
        static AutoCompleteUserInputsAddress autocompleteSendFile = new AutoCompleteUserInputsAddress(UserInputSendFileTXTfile);

        const string UserInputAddressTXTfile = "autoCompleteAddress.txt";
        const string UserInputValuesTXTfile = "autoCompleteValues.txt";
        static AutoCompleteUserInputsAddress autocompleteAddress = new AutoCompleteUserInputsAddress(UserInputAddressTXTfile);
        static AutoCompleteUserInputsAddress autocompleteValue = new AutoCompleteUserInputsAddress(UserInputValuesTXTfile);

        string linkageNumber = "", micronLocation = "", PCS_DL_In_UL_Out = "", PCS_UL_In_DL_Out_1 = "", PCS_UL_In_DL_Out_2 = "",
            DL_Sub_Channel_0_Bandwidth = "", DL_FDD_Center_Frequency = "", Sub_Channel_0_DL_Center_Frequency_Offset = "", Local_En = "",
            transmit_zeros_from_BF = "", UL_FDD_Center_Frequency = "", Sub_Channel_0_UL_Center_Frequency_Offset = "", UL_Sub_Channel_0_Bandwidth = "",
            AGC_BACKOFF = "", AGC_average_shift = "", enabled_Chains = "";


        List<Block> blocksList = new List<Block>();

        public Main()
        {
            InitializeComponent();
            Micron_locationlbl.MouseHover += Micron_locationlbl_MouseHover;
            PCS_DL_In_UL_Outlbl.MouseHover += PCS_DL_In_UL_Outlbl_MouseHover;
            PCS_UL_In_DL_Out_1lbl.MouseHover += PCS_UL_In_DL_Out_1lbl_MouseHover;
            PCS_UL_In_DL_Out_2lbl.MouseHover += PCS_UL_In_DL_Out_2lbl_MouseHover;
            DL_Sub_Channel_0lbl.MouseHover += DL_Sub_Channel_0lbl_MouseHover;
            DLFDDCenterFrequencylbl.MouseHover += DLFDDCenterFrequencylbl_MouseHover;
            Sub_Channel_0DL_CenterFrequencyOffsetlbl.MouseHover += Sub_Channel_0DL_CenterFrequencyOffsetlbl_MouseHover;
            Local_Enlbl.MouseHover += Local_Enlbl_MouseHover;
            Transmit_zeros_from_BFlbl.MouseHover += Transmit_zeros_from_BFlbl_MouseHover;
            ULFDDCenterFrequencylbl.MouseHover += ULFDDCenterFrequencylbl_MouseHover;
            Sub_Channel_0UL_CenterFrequencyOffsetlbl.MouseHover += Sub_Channel_0UL_CenterFrequencyOffsetlbl_MouseHover;
            UL_Sub_Channel_0_Bandwidthlbl.MouseHover += UL_Sub_Channel_0_Bandwidthlbl_MouseHover;
            AGCBACKOFFlbl.MouseHover += AGCBACKOFFlbl_MouseHover;
            AGCaverageshiftlbl.MouseHover += AGCaverageshiftlbl_MouseHover;

        }

        private void Main_Load(object sender, EventArgs e)
        {

            //Version - November 22

            createBlockMemFromUI("..\\..\\Hfiles\\bf.h", 0x120000, Block.eBlockNames.BF, "");
            createBlockMemFromUI("..\\..\\Hfiles\\comm_hub.h", 0x100000, Block.eBlockNames.COMM_HUB, "");
            createBlockMemFromUI("..\\..\\Hfiles\\top.h", 0x140000, Block.eBlockNames.TOP, "");
            createBlockMemFromUI("..\\..\\Hfiles\\calib.h", 0x160000, Block.eBlockNames.CALIB, "");

            string dfeFilePath = "..\\..\\Hfiles\\dfe.h";
            createBlockMemFromUI(dfeFilePath, 0x0, Block.eBlockNames.DFE0, "RX");
            createBlockMemFromUI(dfeFilePath, 0x8000, Block.eBlockNames.DFE0, "TX");

            createBlockMemFromUI(dfeFilePath, 0x10000, Block.eBlockNames.DFE1, "RX");
            createBlockMemFromUI(dfeFilePath, 0x18000, Block.eBlockNames.DFE1, "TX");

            createBlockMemFromUI(dfeFilePath, 0x20000, Block.eBlockNames.DFE2, "RX");
            createBlockMemFromUI(dfeFilePath, 0x28000, Block.eBlockNames.DFE2, "TX");

            createBlockMemFromUI(dfeFilePath, 0x30000, Block.eBlockNames.DFE3, "RX");
            createBlockMemFromUI(dfeFilePath, 0x38000, Block.eBlockNames.DFE3, "TX");

            createBlockMemFromUI(dfeFilePath, 0x40000, Block.eBlockNames.DFE4, "RX");
            createBlockMemFromUI(dfeFilePath, 0x48000, Block.eBlockNames.DFE4, "TX");

            createBlockMemFromUI(dfeFilePath, 0x50000, Block.eBlockNames.DFE5, "RX");
            createBlockMemFromUI(dfeFilePath, 0x58000, Block.eBlockNames.DFE5, "TX");

            createBlockMemFromUI(dfeFilePath, 0x60000, Block.eBlockNames.DFE6, "RX");
            createBlockMemFromUI(dfeFilePath, 0x68000, Block.eBlockNames.DFE6, "TX");

            createBlockMemFromUI(dfeFilePath, 0x70000, Block.eBlockNames.DFE7, "RX");
            createBlockMemFromUI(dfeFilePath, 0x78000, Block.eBlockNames.DFE7, "TX");

            createBlockMemFromUI(dfeFilePath, 0x80000, Block.eBlockNames.DFE8, "RX");
            createBlockMemFromUI(dfeFilePath, 0x88000, Block.eBlockNames.DFE8, "TX");

            createBlockMemFromUI(dfeFilePath, 0x90000, Block.eBlockNames.DFE9, "RX");
            createBlockMemFromUI(dfeFilePath, 0x98000, Block.eBlockNames.DFE9, "TX");

            createBlockMemFromUI(dfeFilePath, 0xA0000, Block.eBlockNames.DFE10, "RX");
            createBlockMemFromUI(dfeFilePath, 0xA8000, Block.eBlockNames.DFE10, "TX");

            createBlockMemFromUI(dfeFilePath, 0xB0000, Block.eBlockNames.DFE11, "RX");
            createBlockMemFromUI(dfeFilePath, 0xB8000, Block.eBlockNames.DFE11, "TX");

            createBlockMemFromUI(dfeFilePath, 0xC0000, Block.eBlockNames.DFE12, "RX");
            createBlockMemFromUI(dfeFilePath, 0xC8000, Block.eBlockNames.DFE12, "TX");

            createBlockMemFromUI(dfeFilePath, 0xD0000, Block.eBlockNames.DFE13, "RX");
            createBlockMemFromUI(dfeFilePath, 0xD8000, Block.eBlockNames.DFE13, "TX");

            createBlockMemFromUI(dfeFilePath, 0xE0000, Block.eBlockNames.DFE14, "RX");
            createBlockMemFromUI(dfeFilePath, 0xE8000, Block.eBlockNames.DFE14, "TX");

            createBlockMemFromUI(dfeFilePath, 0xF0000, Block.eBlockNames.DFE15, "RX");
            createBlockMemFromUI(dfeFilePath, 0xF8000, Block.eBlockNames.DFE15, "TX");

            comboBoxLoadPorts();
            TabControl1.Location = new Point(10, pictureBox1.Height + 5);
            TabControl1.Width = this.ClientRectangle.Width - 20;
            TabControl1.Height = this.ClientRectangle.Height - pictureBox1.Height - 20;

            buttonOpen.Location = new Point(3, blocksPanel.ClientSize.Height);


            //Block tab
            blocksPanel.Padding = new Padding(2, 5, 10, 2);
            int Ypoint = 10;
            int Xpoint = 5;
            int countToHalf = 0;
            foreach (Block.eBlockNames block_name in Block.eBlockNames.GetValues(typeof(Block.eBlockNames)))
            {
                CheckBox checkbox1 = new CheckBox();
                checkbox1.Text = block_name.ToString();
                checkbox1.Size = new Size(180, 35);
                countToHalf++;
                if (countToHalf == 9)
                {
                    Xpoint = 220;
                    Ypoint = 10;
                }
                if (countToHalf == 17)
                {
                    Xpoint = 400;
                    Ypoint = 10;

                }
                if (countToHalf == 25)
                {
                    Xpoint = 590;
                    Ypoint = 10;
                }
                checkbox1.Location = new Point(Xpoint, Ypoint);
                blocksPanel.Controls.Add(checkbox1);
                Ypoint += 40;

            }
        }




        // Blocks Tab

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            if (isComConfig)
            {
                Button clickedBtn = sender as Button;
                var checkBoxesList = blocksPanel.Controls.OfType<CheckBox>();
                List<CheckBox> checkBoxesCheckedList = new List<CheckBox>();
                foreach (CheckBox cb in checkBoxesList)
                {
                    if (cb.Checked)
                        checkBoxesCheckedList.Add(cb);

                }
                Block Tx_Block = null, Rx_Block = null;

                for (int i = 0; i < checkBoxesCheckedList.Count; i++)
                {
                    string blockCheckBoxName = checkBoxesCheckedList[i].Text.ToString();
                    foreach (Block b in blocksList)
                    {
                        if (b.BlockName.ToString().Contains("DFE"))
                        {
                            if (b.RxOrtx.Equals("RX"))
                                Rx_Block = b;
                            else if (b.RxOrtx.Equals("TX"))
                                Tx_Block = b;
                            if (Tx_Block != null && Rx_Block != null && b.BlockName.ToString() == blockCheckBoxName)
                            {
                                GeneralDFEBlock generalDFEBlock = new GeneralDFEBlock(Tx_Block, Rx_Block, ref serialPort1);
                                generalDFEBlock.Text = b.BlockName.ToString();
                                generalDFEBlock.Show();
                                Tx_Block = null;
                                Rx_Block = null;
                            }
                        }
                        else if (b.BlockName.ToString() == blockCheckBoxName)
                        {
                            generalBlock generalBlock = new generalBlock(b, ref serialPort1);
                            generalBlock.Text = b.BlockName.ToString();
                            generalBlock.Show();

                        }
                    }
                }

                foreach (CheckBox cb in checkBoxesList)
                {
                    if (cb.Checked)
                        cb.Checked = false;

                }

            }
        }






        // Terminal Tab

        private void ComComboBox_Click(object sender, EventArgs e)
        {
            comboBoxLoadPorts();
        }

        private void comboBoxLoadPorts()
        {
            ComComboBox.Items.Clear();
            var portnames = SerialPort.GetPortNames();
            if (portnames.Length > 0)
                ComComboBox.DropDownHeight = portnames.Length * (ComComboBox.ItemHeight);
            foreach (string portname in portnames)
                ComComboBox.Items.Add(portname);
        }

        public string ReadFromPort()
        {
            Boolean flagToNothing = false;
            int timeOFtimeout = 0;
            serialPort1.ReadTimeout = 100;
            string answerLineFromPort = "";
            while (!answerLineFromPort.Contains(EOM))
            {
                try
                {
                    int numberOfBytes = serialPort1.BytesToRead;
                    if (numberOfBytes > 0)
                        answerLineFromPort += serialPort1.ReadExisting();
                }
                catch (TimeoutException ex)
                {
                    timeOFtimeout++;

                    if (timeOFtimeout > 5)
                    {
                        flagToNothing = true;
                        break;
                    }
                    else
                        continue;
                }
            }
            if (flagToNothing) return "Communication lost"; else return answerLineFromPort;
        }
        public void WriteToPort(string command)
        {
            string reply;
            if (command.Contains("\r\n")) serialPort1.Write($"{command}");
            else
            {
                if (command.Contains("\r"))
                {
                    command = command + "\n";
                    serialPort1.Write(command);
                }
                else if (!command.Contains("\r") && !(command.Contains("\\n")))
                {
                    command = command + "\r\n";
                    serialPort1.Write(command);
                }
            }

            DateTime timestamp = DateTime.Now;
            PrintSendMessage(terminalTextBox, command, timestamp);
            reply = ReadFromPort();
            timestamp = DateTime.Now;
            PrintRecieveMessage(terminalTextBox, reply, timestamp);
        }

        //Send Text & File
        private void sendTextBtn_Click(object sender, EventArgs e)
        {
            if (isComConfig)
            {
                if (sendTextTextBox.Text != "")
                {
                    try
                    {
                        WriteToPort($"{sendTextTextBox.Text.ToString()}\r\n");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"{ex}");
                    }
                    autocompleteSendText.Open();
                    autocompleteSendText.AddValue(sendTextTextBox.Text);
                    sendTextTextBox.AutoCompleteCustomSource.AddRange(autocompleteSendText.returnListUserInputAddress());

                }
                else
                {
                    MessageBox.Show("Please enter a text to send.");
                }
            }
            else
            {
                MessageBox.Show("Please connect to COM port please.");
            }
        }
        private void sendTextTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyData == Keys.Enter) && (sendTextTextBox.Text != ""))
            {
                sendTextBtn.PerformClick();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
        private void LoadTextFileBtn_Click(object sender, EventArgs e)
        {
            string strfilename = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strfilename = openFileDialog.FileName;
                sendFileTextBox.Text = strfilename;
            }
        }

        private void sendFileBtn_Click(object sender, EventArgs e)
        {
            string filePath = sendFileTextBox.Text.ToString();
            using (StreamReader sr = new StreamReader(filePath))
            {
                string[] commandsToWrite = sr.ReadToEnd().Trim().Split('\n');
                for (int i = 0; i < commandsToWrite.Length; i++)
                {
                    WriteToPort(commandsToWrite[i]);
                    Thread.Sleep(10);
                }
            }
        }
        private void sendFileTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyData == Keys.Enter) && (sendFileTextBox.Text != ""))
            {
                autocompleteSendFile.Open();
                autocompleteSendFile.AddValue(sendFileTextBox.Text);
                sendTextTextBox.AutoCompleteCustomSource.AddRange(autocompleteSendFile.returnListUserInputAddress());
                sendFileBtn.PerformClick();
                e.Handled = true;
                e.SuppressKeyPress = true;

            }
        }


        //Configuration To COM Port
        private void OpenClosePortBtn_Click(object sender, EventArgs e)
        {
            int bitsPerSecond = 115200;
            if (OpenClosePortBtn.Text.Equals("Open"))
            {
                if (ComComboBox.SelectedItem != null)
                {
                    if (BitsPerSecondsComboBox.SelectedItem != null) bitsPerSecond = Int32.Parse(BitsPerSecondsComboBox.SelectedItem.ToString());
                    try
                    {
                        if (serialPort1 != null)
                        {
                            serialPort1.PortName = ComComboBox.SelectedItem.ToString();
                            serialPort1.Open();
                        }
                        else
                        {
                            serialPort1 = new SerialPort($"{ComComboBox.SelectedItem.ToString()}", bitsPerSecond, Parity.None, 8, StopBits.One);
                            serialPort1.Open();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"{ex}");
                    }
                    panel1.BackColor = Color.Green;
                    isComConfig = true;
                    OpenClosePortBtn.Text = "Close";
                }
                else
                {
                    MessageBox.Show("Please Choose COM port.");
                }
            }
            else if (OpenClosePortBtn.Text.Equals("Close"))
            {
                if (serialPort1.IsOpen)
                    serialPort1.Close();
                panel1.BackColor = Color.Red;
                isComConfig = false;
                OpenClosePortBtn.Text = "Open";

            }
        }

        private void clearTerminalBtn_Click(object sender, EventArgs e)
        {
            terminalTextBox.Clear();
        }


        //init blocks

        private void createBlockMemFromUI(string hFilePath, int baseAddress, Block.eBlockNames BlockName, string rxOrtx)
        {
            List<string> registers_name_list = new List<string>();
            List<string> registers_address_list = new List<string>();

            List<Register> registersMemList = new List<Register>();
            Block block;

            int registerNum = -1;
            using (var reader = new StreamReader($"{hFilePath}"))
            {
                string[] linesfrombHfile = reader.ReadToEnd().Split('\n');

                for (int Numofline = 0; Numofline < linesfrombHfile.Length - 1; Numofline++)
                {
                    if (linesfrombHfile[Numofline].Contains("//module typedef"))
                        break;
                    else
                    {
                        if (linesfrombHfile[Numofline].Contains("//") || linesfrombHfile[Numofline].Contains("/*"))
                            continue;
                        else
                        {
                            if (linesfrombHfile[Numofline].Contains("#define"))
                            {
                                int indexOfDefine = linesfrombHfile[Numofline].IndexOf("#define ");
                                int indexOfOffsetWord = linesfrombHfile[Numofline].IndexOf("_OFFSET");
                                string registerName = "";
                                for (int i = indexOfDefine + 8; i < indexOfOffsetWord; i++)
                                {
                                    registerName += linesfrombHfile[Numofline][i];
                                }
                                registers_name_list.Add(registerName);

                                string registerHalfAddress = "";
                                int indexOfHexPrefix = linesfrombHfile[Numofline].IndexOf("(0x");
                                int indexOfRightParanthesis = linesfrombHfile[Numofline].IndexOf(")");
                                for (int i = indexOfHexPrefix + 3; i < indexOfRightParanthesis; i++)
                                {
                                    registerHalfAddress += linesfrombHfile[Numofline][i];
                                }
                                int registerHalgAddressInt = Convert.ToInt32(registerHalfAddress, 16);
                                int registerFullAddress = baseAddress + registerHalgAddressInt;
                                string registerFullAddressString = $"0x{registerFullAddress.ToString("X")}";
                                registers_address_list.Add(registerFullAddressString);

                                continue;
                            }
                            if (linesfrombHfile[Numofline].Contains("typedef struct"))
                            {
                                Numofline += 2;
                                registerNum++;

                                List<Field> fieldsList = new List<Field>();


                                string addressString = registers_address_list[registerNum];

                                int num_of_fields_on_struct = 0;
                                int last_field_location = 30;
                                int sumOfBits = 0;
                                while (!linesfrombHfile[Numofline].Contains("}"))
                                {
                                    num_of_fields_on_struct++;


                                    string fieldName = "";
                                    string fieldBitsString = "";
                                    int fieldBits = 0;

                                    int IndexOfUnsigned = linesfrombHfile[Numofline].IndexOf("unsigned");
                                    int indexOfColon = linesfrombHfile[Numofline].IndexOf(":");
                                    for (int n = IndexOfUnsigned + 9; n < indexOfColon; n++)
                                    {
                                        fieldName += linesfrombHfile[Numofline][n].ToString();
                                    }
                                    for (int n = indexOfColon + 1; n < linesfrombHfile[Numofline].Length - 1; n++)
                                    {
                                        string bbb = linesfrombHfile[Numofline][n].ToString();
                                        if (bbb.Equals(";"))
                                            break;
                                        fieldBitsString += bbb.ToString();
                                    }
                                    fieldBits = Int32.Parse(fieldBitsString);
                                    sumOfBits += fieldBits;


                                    Field m_field = new Field(fieldName, fieldBits, 0, 0, "");
                                    fieldsList.Add(m_field);



                                    Numofline++;
                                }

                                foreach (Field field2 in fieldsList)
                                {
                                    field2.endIndex = sumOfBits - 1;
                                    field2.startIndex = sumOfBits - field2.fieldBits;
                                    sumOfBits -= field2.fieldBits;
                                }

                                Register register = new Register(registers_name_list[registerNum].ToString(), BlockName, Convert.ToInt32(addressString, 16), "", fieldsList);
                                registersMemList.Add(register);
                            }
                        }
                    }
                }


            }

            block = new Block(BlockName, registersMemList, baseAddress);
            if (rxOrtx != "")
            {
                block.RxOrtx = rxOrtx;
            }
            blocksList.Add(block);


        }

        //Panel Buttons

        private void StartModeBtn_Click(object sender, EventArgs e)
        {
            if ((isComConfig) && (NtoStartModetb.Text != ""))
            {
                double nToStartMode = Convert.ToDouble(NtoStartModetb.Text);
                try
                {
                    if (!(serialPort1.IsOpen))
                        serialPort1.Open();

                    serialPort1.WriteTimeout = 100;

                    string commandToWrite = "fwupd sysinfo\r\n";
                    PrintSendMessage(terminalTextBox, commandToWrite, DateTime.Now);

                    serialPort1.Write($"{commandToWrite}");

                }

                catch (TimeoutException ex)
                {
                    MessageBox.Show("Error:", ex.Message);
                }
                string reply = ReadFromPort();
                if (reply.Contains("OK"))
                {
                    PrintRecieveMessage(terminalTextBox, reply, DateTime.Now);
                    try
                    {
                        if (!(serialPort1.IsOpen))
                            serialPort1.Open();

                        serialPort1.WriteTimeout = 100;

                        string commandToWrite = "fpga getspiconf\r\n";
                        PrintSendMessage(terminalTextBox, commandToWrite, DateTime.Now);

                        serialPort1.Write($"{commandToWrite}");

                    }

                    catch (TimeoutException ex)
                    {
                        MessageBox.Show("Error:", ex.Message);
                    }

                    reply = ReadFromPort();
                    PrintRecieveMessage(terminalTextBox, reply, DateTime.Now);
                }


            }
        }
        private void resetFPGAbtn_Click(object sender, EventArgs e)
        {
            if (isComConfig)
            {
                if (!(serialPort1.IsOpen))
                    serialPort1.Open();
                try
                {
                    string commandToWrite = $"fpga resetspi rst1\r\n";
                    //PrintSendMessage(terminalTextBox, commandToWrite, DateTime.Now);
                    serialPort1.Write(commandToWrite);
                }
                catch (TimeoutException ex)
                {
                    MessageBox.Show("Error:", ex.Message);
                }
                string reply = ReadFromPort();
                PrintStartModeMessage(terminalTextBox, "Reset FPGA", DateTime.Now);
            }
            else
            {
                MessageBox.Show("Please connect to COM port first", "Error!");
            }
        }

        private void resetSPIbtn_Click(object sender, EventArgs e)
        {
            if (isComConfig)
            {
                if (!(serialPort1.IsOpen))
                    serialPort1.Open();
                try
                {
                    string commandToWrite = $"fpga resetspi rst2\r\n";
                    //PrintSendMessage(terminalTextBox, commandToWrite, DateTime.Now);
                    serialPort1.Write(commandToWrite);
                }
                catch (TimeoutException ex)
                {
                    MessageBox.Show("Error:", ex.Message);
                }
                string reply = ReadFromPort();
                PrintStartModeMessage(terminalTextBox, "Reset SPI", DateTime.Now);
            }
            else
            {
                MessageBox.Show("Please connect to COM port first", "Error!");
            }
        }
        private void readAllBlocksBtn_Click(object sender, EventArgs e)
        {
            if (isComConfig)
            {
                string m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\";
                StreamWriter outputFile;
                outputFile = new StreamWriter(Path.Combine(m_exePath, $"{DateTime.Now.ToString("dd_MM_yy HH_mm_ss")},ReadAllBlockLog.txt"));

                foreach (Block block in blocksList)
                {
                    foreach(Register register in block.RegistersList)
                    {
                        serialPort1.Write($"{READ_REG}{register.addressOffset}\r\n");
                        string reply = ReadFromPort();
                        if (reply.Contains("OK"))
                        {
                            string value= parseValueFromReply(reply);
                            outputFile.WriteLine($"{WRITE_REG}0x{register.addressOffset.ToString("X")} {value}");
                            outputFile.WriteLine($"{READ_REG}0x{register.addressOffset.ToString("X")}");
                        }
                        else
                        {

                        }
                    }
                }
            }
        }
        public string parseValueFromReply(string reply)
        {
            string value = "";
            string[] rows = reply.Split('\n');
            foreach(string row in rows)
            {
                if (row.Contains("Value"))
                {
                    
                    int indexOfValue = row.IndexOf("Value=");
                    for (int i=indexOfValue+6; i<row.Length-1; i++)
                    {
                        value += row[i];
                    }
                }
                else continue;
            }
            return value;
        }

        //UI changes

        public delegate void ChangeTerminalTextBoxCallback(RichTextBox terminal, string answerFromPort, DateTime timestamp);
        public void PrintRecieveMessage(RichTextBox terminal, string answerFromPort, DateTime timestamp)
        {
            if (terminal.InvokeRequired)
            {
                ChangeTerminalTextBoxCallback call = new ChangeTerminalTextBoxCallback(PrintRecieveMessage);
                terminal.BeginInvoke(call, terminal, answerFromPort);
            }
            else
            {
                terminal.SelectionColor = Color.SeaGreen;
                terminal.AppendText($"<{timestamp.ToString("dd/MM/yy, HH:mm:ss")} Recieved> \n");
                terminal.SelectionColor = Color.Black;
                terminal.AppendText($"{answerFromPort}\r\n");
                terminal.ScrollToCaret();
            }
        }
        public void PrintSendMessage(RichTextBox terminal, string commandToWrite, DateTime timestamp)
        {
            if (terminal.InvokeRequired)
            {
                ChangeTerminalTextBoxCallback call = new ChangeTerminalTextBoxCallback(PrintSendMessage);
                terminal.BeginInvoke(call, terminal, commandToWrite);
            }
            else
            {
                terminal.SelectionColor = Color.Purple;
                terminal.AppendText($"<{timestamp.ToString("dd/MM/yy, HH:mm:ss")} Sent> \n");
                terminal.SelectionColor = Color.Black;
                terminal.AppendText($"{commandToWrite}");
            }
        }
        public void PrintStartModeMessage(RichTextBox terminal, string reply, DateTime timestamp)
        {
            if (terminal.InvokeRequired)
            {
                ChangeTerminalTextBoxCallback call = new ChangeTerminalTextBoxCallback(PrintStartModeMessage);
                terminal.BeginInvoke(call, terminal, reply);
            }
            else
            {
                terminal.SelectionColor = Color.Purple;
                terminal.AppendText($"<{timestamp.ToString("dd/MM/yy, HH:mm:ss")} Print> \n");
                terminal.SelectionColor = Color.Black;
                terminal.AppendText($"{reply}\n");
            }
        }





        // High level Parameters Tab


        //Save & Load configuration

        private void saveConfigToExcel_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            //build Excel file
            xlWorkSheet.Cells[1, 1] = "Order by GUI";
            xlWorkSheet.Cells[1, 2] = "Order By Command";
            xlWorkSheet.Cells[1, 3] = "Value";
            Button clickedButton = sender as Button;
            var textBoxList = clickedButton.Parent.Controls.OfType<TextBox>();
            foreach (TextBox tb in textBoxList)
            {
                if (tb.Name.Equals("Linkage_numbertb"))
                {
                    xlWorkSheet.Cells[2, 1] = "01 Linkage Number";
                    xlWorkSheet.Cells[2, 2] = "1";
                    xlWorkSheet.Cells[2, 3] = tb.Text;
                }
                else
                {
                    if (tb.Name.Equals("Micron_locationtb"))
                    {
                        xlWorkSheet.Cells[3, 1] = "02 Micron Location ";
                        xlWorkSheet.Cells[3, 2] = "2";
                        xlWorkSheet.Cells[3, 3] = tb.Text;
                    }
                    else
                    {
                        if (tb.Name.Equals("PCS_DL_In_UL_Outtb"))
                        {
                            xlWorkSheet.Cells[4, 1] = "03 PCS DL In UL Out ";
                            xlWorkSheet.Cells[4, 2] = "3";
                            xlWorkSheet.Cells[4, 3] = tb.Text;
                        }
                        else
                        {
                            if (tb.Name.Equals("PCS_UL_In_DL_Out_1tb"))
                            {
                                xlWorkSheet.Cells[5, 1] = "04 PCS UL In DL Out 1 ";
                                xlWorkSheet.Cells[5, 2] = "4";
                                xlWorkSheet.Cells[5, 3] = tb.Text;
                            }
                            else
                            {
                                if (tb.Name.Equals("PCS_UL_In_DL_Out_2tb"))
                                {
                                    xlWorkSheet.Cells[6, 1] = "05 PCS UL In DL Out 2";
                                    xlWorkSheet.Cells[6, 2] = "5";
                                    xlWorkSheet.Cells[6, 3] = tb.Text;
                                }
                                else
                                {
                                    if (tb.Name.Equals("DL_Sub_Channel_0_Bandwidthtb"))
                                    {
                                        xlWorkSheet.Cells[7, 1] = "06 DL SubChannel 0 Bandwidth";
                                        xlWorkSheet.Cells[7, 2] = "7";
                                        xlWorkSheet.Cells[7, 3] = tb.Text;
                                    }
                                    else
                                    {
                                        if (tb.Name.Equals("UL_Sub_Channel_0_Bandwidthtb"))
                                        {
                                            xlWorkSheet.Cells[8, 1] = "07 UL SubChannel 0 Bandwidth";
                                            xlWorkSheet.Cells[8, 2] = "14";
                                            xlWorkSheet.Cells[8, 3] = tb.Text;
                                        }
                                        else
                                        {
                                            if (tb.Name.Equals("DLFDDCenterFrequencytb"))
                                            {
                                                xlWorkSheet.Cells[9, 1] = "08 DL FDD Center Frequency";
                                                xlWorkSheet.Cells[9, 2] = "8";
                                                xlWorkSheet.Cells[9, 3] = tb.Text;
                                            }
                                            else
                                            {
                                                if (tb.Name.Equals("Sub_Channel_0DL_CenterFrequencyOffsettb"))
                                                {
                                                    xlWorkSheet.Cells[10, 1] = "09 SubChannel 0 DL Center Frequency Offset";
                                                    xlWorkSheet.Cells[10, 2] = "6";
                                                    xlWorkSheet.Cells[10, 3] = tb.Text;
                                                }
                                                else
                                                {
                                                    if (tb.Name.Equals("ULFDDCenterFrequencytb"))
                                                    {
                                                        xlWorkSheet.Cells[11, 1] = "10 UL FDD Center Frequency";
                                                        xlWorkSheet.Cells[11, 2] = "15";
                                                        xlWorkSheet.Cells[11, 3] = tb.Text;
                                                    }
                                                    else
                                                    {
                                                        if (tb.Name.Equals("Sub_Channel_0UL_CenterFrequencyOffsettb"))
                                                        {
                                                            xlWorkSheet.Cells[12, 1] = "11 SubChannel 0 UL Center Frequency Offset";
                                                            xlWorkSheet.Cells[12, 2] = "13";
                                                            xlWorkSheet.Cells[12, 3] = tb.Text;
                                                        }
                                                        else
                                                        {
                                                            if (tb.Name.Equals("Local_Entb"))
                                                            {
                                                                xlWorkSheet.Cells[13, 1] = "12 Local En";
                                                                xlWorkSheet.Cells[13, 2] = "9";
                                                                xlWorkSheet.Cells[13, 3] = tb.Text;
                                                            }
                                                            else
                                                            {
                                                                if (tb.Name.Equals("Transmit_zeros_from_BFtb"))
                                                                {
                                                                    xlWorkSheet.Cells[14, 1] = "13 Transmit zeros from BF";
                                                                    xlWorkSheet.Cells[14, 2] = "10";
                                                                    xlWorkSheet.Cells[14, 3] = tb.Text;
                                                                }
                                                                else
                                                                {
                                                                    if (tb.Name.Equals("AGCBACKOFFtb"))
                                                                    {
                                                                        xlWorkSheet.Cells[15, 1] = "14 AGC BACKOFF";
                                                                        xlWorkSheet.Cells[15, 2] = "11";
                                                                        xlWorkSheet.Cells[15, 3] = tb.Text;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (tb.Name.Equals("AGCaverageshiftyb"))
                                                                        {
                                                                            xlWorkSheet.Cells[16, 1] = "15 AGC average shift";
                                                                            xlWorkSheet.Cells[16, 2] = "12";
                                                                            xlWorkSheet.Cells[16, 3] = tb.Text;
                                                                        }
                                                                        else
                                                                        {
                                                                            if (tb.Name.Equals("enabledchaninstb"))
                                                                            {
                                                                                xlWorkSheet.Cells[17, 1] = "16 Enabled chains (antennas)";
                                                                                xlWorkSheet.Cells[17, 2] = "16";
                                                                                xlWorkSheet.Cells[17, 3] = tb.Text;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "CSV Files (L*.csv)|L*.csv";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filename = saveFileDialog.FileName;
                    xlWorkBook.SaveAs($"{filename}", Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Saving Excel file failed. {ex}", "Saving Excel file failed");
            }

            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
            Cursor.Current = Cursors.Default;

        }
        private void LoadConfigurationFileBtn()
        {
            if (textBoxExcelpath.Text != "")
            {
                using (var reader = new StreamReader($"{textBoxExcelpath.Text}"))
                {
                    List<string> listInputs = new List<string>();
                    reader.ReadLine();

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');

                        for (int i = 0; i < 2; i++)
                            listInputs.Add(values[i]);


                        if (values[0].Contains("Linkage Number"))
                            Linkage_numbertb.Text = values[2];
                        else
                        {
                            if (values[0].Contains("Micron Location "))
                                Micron_locationtb.Text = values[2];
                            else
                            {
                                if (values[0].Contains("PCS DL In UL Out "))
                                    PCS_DL_In_UL_Outtb.Text = values[2];
                                else
                                {
                                    if (values[0].Contains("PCS UL In DL Out 1 "))
                                        PCS_UL_In_DL_Out_1tb.Text = values[2];
                                    else
                                    {
                                        if (values[0].Contains("PCS UL In DL Out 2"))
                                            PCS_UL_In_DL_Out_2tb.Text = values[2];
                                        else
                                        {
                                            if (values[0].Contains("DL SubChannel 0 Bandwidth"))
                                                DL_Sub_Channel_0_Bandwidthtb.Text = values[2];
                                            else
                                            {
                                                if (values[0].Contains("DL FDD Center Frequency"))
                                                    DLFDDCenterFrequencytb.Text = values[2];
                                                else
                                                {
                                                    if (values[0].Contains("SubChannel 0 DL Center Frequency Offset"))
                                                        Sub_Channel_0DL_CenterFrequencyOffsettb.Text = values[2];
                                                    else
                                                    {
                                                        if (values[0].Contains("Local En"))
                                                            Local_Entb.Text = values[2];
                                                        else
                                                        {
                                                            if (values[0].Contains("Transmit zeros from BF"))
                                                                Transmit_zeros_from_BFtb.Text = values[2];
                                                            else
                                                            {
                                                                if (values[0].Contains("UL FDD Center Frequency"))
                                                                    ULFDDCenterFrequencytb.Text = values[2];
                                                                else
                                                                {
                                                                    if (values[0].Contains("SubChannel 0 UL Center Frequency Offset"))
                                                                        Sub_Channel_0UL_CenterFrequencyOffsettb.Text = values[2];
                                                                    else
                                                                    {
                                                                        if (values[0].Contains("UL SubChannel 0 Bandwidth"))
                                                                            UL_Sub_Channel_0_Bandwidthtb.Text = values[2];
                                                                        else
                                                                        {
                                                                            if (values[0].Contains("AGC BACKOFF"))
                                                                                AGCBACKOFFtb.Text = values[2];
                                                                            else
                                                                            {
                                                                                if (values[0].Contains("AGC average shift"))
                                                                                    AGCaverageshiftyb.Text = values[2];
                                                                                else
                                                                                {
                                                                                    if (values[0].Contains("Enabled chains (antennas)"))
                                                                                        enabledchaninstb.Text = values[2];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }



                        }

                    }
                }
            }
            else
            {
                MessageBox.Show("Please choose a file before loading!", "Error!");
            }
        }

        private void browseBtn_Click(object sender, EventArgs e)
        {
            string strfilename = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                strfilename = openFileDialog.FileName;
                textBoxExcelpath.Text = strfilename;
            }

            LoadConfigurationFileBtn();
        }
        private void clearBtn_Click(object sender, EventArgs e)
        {
            ConsolePrintTB.Clear();
        }

        // Excecute
        private void button1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Button clickedBTN = sender as Button;
            var listOfTextBox = clickedBTN.Parent.Controls.OfType<TextBox>();
            bool checkIfAllFieldsFill = true;

            foreach (TextBox tb in listOfTextBox)
            {
                if (tb.Text.Equals("") && !tb.Name.Equals("ConsolePrintTB") && !tb.Name.Equals("textBoxExcelpath") && !tb.Name.Equals("readAdressTB") && !tb.Name.Equals("writeAddressTB") && !tb.Name.Equals("valueToWriteTB") && !tb.Name.Equals("valueFromReadingTextBox") && !tb.Name.Equals("ValueFromWriingTextBox"))
                    checkIfAllFieldsFill = false;
                else
                {
                    if (tb.Name.Equals("Linkage_numbertb"))
                        linkageNumber = tb.Text;
                    else
                    {
                        if (tb.Name.Equals("Micron_locationtb"))
                            micronLocation = tb.Text;
                        else
                        {
                            if (tb.Name.Equals("PCS_DL_In_UL_Outtb"))
                                PCS_DL_In_UL_Out = tb.Text;
                            else
                            {
                                if (tb.Name.Equals("PCS_UL_In_DL_Out_1tb"))
                                    PCS_UL_In_DL_Out_1 = tb.Text;
                                else
                                {
                                    if (tb.Name.Equals("PCS_UL_In_DL_Out_2tb"))
                                        PCS_UL_In_DL_Out_2 = tb.Text;
                                    else
                                    {
                                        if (tb.Name.Equals("UL_Sub_Channel_0_Bandwidthtb"))
                                            UL_Sub_Channel_0_Bandwidth = tb.Text;
                                        else
                                        {
                                            if (tb.Name.Equals("DLFDDCenterFrequencytb"))
                                                DL_FDD_Center_Frequency = tb.Text;
                                            else
                                            {
                                                if (tb.Name.Equals("Sub_Channel_0DL_CenterFrequencyOffsettb"))
                                                    Sub_Channel_0_DL_Center_Frequency_Offset = tb.Text;
                                                else
                                                {
                                                    if (tb.Name.Equals("Local_Entb"))
                                                        Local_En = tb.Text;
                                                    else
                                                    {
                                                        if (tb.Name.Equals("Transmit_zeros_from_BFtb"))
                                                            transmit_zeros_from_BF = tb.Text;
                                                        else
                                                        {
                                                            if (tb.Name.Equals("ULFDDCenterFrequencytb"))
                                                                UL_FDD_Center_Frequency = tb.Text;
                                                            else
                                                            {
                                                                if (tb.Name.Equals("Sub_Channel_0UL_CenterFrequencyOffsettb"))
                                                                    Sub_Channel_0_UL_Center_Frequency_Offset = tb.Text;
                                                                else
                                                                {
                                                                    if (tb.Name.Equals("DL_Sub_Channel_0_Bandwidthtb"))
                                                                        DL_Sub_Channel_0_Bandwidth = tb.Text;
                                                                    else
                                                                    {
                                                                        if (tb.Name.Equals("AGCBACKOFFtb"))
                                                                            AGC_BACKOFF = tb.Text;
                                                                        else
                                                                        {
                                                                            if (tb.Name.Equals("AGCaverageshiftyb"))
                                                                                AGC_average_shift = tb.Text;
                                                                            else
                                                                            {
                                                                                if (tb.Name.Equals("enabledchaninstb"))
                                                                                    enabled_Chains = tb.Text;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }

            if (checkIfAllFieldsFill)
            {
                if (!(serialPort1.IsOpen))
                    serialPort1.Open();

                try
                {
                    string commandToWrite = $"fpga resetspi rst1\r\nfpga resetspi rst2\r\n";
                    serialPort1.Write(commandToWrite);
                }
                catch (TimeoutException ex)
                {
                    MessageBox.Show("Error:", ex.Message);
                }
                string reply = ReadFromPort();
                PrintStartModeMessage(ConsolePrintTB, "Reset FPGA \r\n Reset SPI\r\n", DateTime.Now);
                serialPort1.DiscardInBuffer();

                serialPort1.Write($"fpga_config micron_hl_params_config_function {linkageNumber} {micronLocation} {PCS_DL_In_UL_Out} {PCS_UL_In_DL_Out_1} " +
                    $"{PCS_UL_In_DL_Out_2} {DL_Sub_Channel_0_Bandwidth} {DL_FDD_Center_Frequency} {Sub_Channel_0_DL_Center_Frequency_Offset} " +
                    $"{Local_En} {transmit_zeros_from_BF} {UL_FDD_Center_Frequency} {Sub_Channel_0_UL_Center_Frequency_Offset} {UL_Sub_Channel_0_Bandwidth} " +
                    $"{AGC_BACKOFF} {AGC_average_shift} {enabled_Chains}\r\n");

                string answerLineFromWrite = "";
                serialPort1.ReadTimeout = 100;

                Task reading = Task.Factory.StartNew(() =>
                {
                    while (!answerLineFromWrite.Contains("OK"))
                    {
                        //serialPort1.DiscardInBuffer();
                        int numberOfBytes = serialPort1.BytesToRead;
                        if (numberOfBytes > 0)
                        {
                            answerLineFromWrite = serialPort1.ReadExisting();
                            WriteText(ConsolePrintTB, answerLineFromWrite);
                            Thread.Sleep(100);
                        }
                    }
                    if ((serialPort1.IsOpen))
                        serialPort1.Close();
                });



            }
            else
                MessageBox.Show("There are empty parameters!", "Error!");
            Cursor.Current = Cursors.Default;
        }


        // General read & write
        private void read_general_btn_Click(object sender, EventArgs e)
        {
            string address = "";

            if (readAdressTB.Text != "")
            {
                autocompleteAddress.Open();
                autocompleteAddress.AddValue(readAdressTB.Text);
                readAdressTB.AutoCompleteCustomSource.AddRange(autocompleteAddress.returnListUserInputAddress());
                if (readAdressTB.Text.Contains("0x"))
                    address = readAdressTB.Text;
                else
                    address = "0x" + readAdressTB.Text;
            }
            else
            {
                WriteText(ConsolePrintTB, "Reading register failed. Please enter a valid address");
            }
            if (!serialPort1.IsOpen)
                serialPort1.Open();
            try
            {
                if (address.Contains("\r\n")) serialPort1.Write($"fpga readreg {address}");
                else
                {
                    if (address.Contains("\r") && !address.Contains("\n")) serialPort1.Write($"fpga readreg {address}\n");
                    else if (!address.Contains("\r\n")) serialPort1.Write($"fpga readreg {address}\r\n");
                }

            }
            catch (Exception ex)
            {

            }

            string reply = ReadFromPort();

            if (reply.Contains("OK"))
            {
                string[] rows = reply.Split('\n');
                string relevantLine = "";

                for (int j = 0; j < rows.Length; j++)
                    if (rows[j].Contains("OK"))
                        relevantLine = rows[j - 1];

                for (int i = relevantLine.Length - 1; i > 0; i--)
                {
                    string chh = relevantLine[i].ToString();
                    int length = 0;
                    if (chh.Equals("x"))
                    {

                        int iBefore = i;
                        string ch = relevantLine[i].ToString();
                        while (!(ch.Equals("\r")))
                        {
                            i++;
                            length++;
                            ch = relevantLine[i].ToString();
                        }
                        valueFromReadingTextBox.Text = relevantLine.Substring(iBefore + 1, length - 1);
                        WriteText(ConsolePrintTB, $"read register {address}- value: {valueToWriteTB.Text.ToString()} \n");
                        break;
                    }
                }

            }
            else
            {
                WriteText(ConsolePrintTB, $"Reading register failed. {reply}");
            }
        }

        private void write_general_btn_Click(object sender, EventArgs e)
        {
            string address = "";
            string value = "";

            if (writeAddressTB.Text != "")
            {
                autocompleteAddress.Open();
                autocompleteAddress.AddValue(writeAddressTB.Text);
                writeAddressTB.AutoCompleteCustomSource.AddRange(autocompleteAddress.returnListUserInputAddress());
                if (writeAddressTB.Text.Contains("0x"))
                    address = writeAddressTB.Text;
                else
                    address = "0x" + writeAddressTB.Text;
            }
            else
            {
                WriteText(ConsolePrintTB, "Reading register failed. Please enter a valid address");
            }
            if (valueToWriteTB.Text != "")
            {
                autocompleteValue.Open();
                autocompleteValue.AddValue(valueToWriteTB.Text);
                valueToWriteTB.AutoCompleteCustomSource.AddRange(autocompleteValue.returnListUserInputAddress());
                value = valueToWriteTB.Text.ToString();
            }
            else
                WriteText(ConsolePrintTB, "Writing register failed. Please enter a valid value");

            if (!serialPort1.IsOpen)
                serialPort1.Open();
            try
            {
                if (value.Contains("\r\n")) serialPort1.Write($"fpga writereg {address} {value}");
                else
                {
                    if (value.Contains("\r") && !value.Contains("\n")) serialPort1.Write($"fpga writereg {address} {value}\n");
                    else if (!value.Contains("\r\n")) serialPort1.Write($"fpga writereg {address} {value}\r\n");
                }
            }
            catch (Exception ex)
            {

            }
            string reply = ReadFromPort();


            if (reply.Contains("OK"))
            {
                ValueFromWriingTextBox.Text = valueToWriteTB.Text.ToString();
                WriteText(ConsolePrintTB, $"write register {address}- value: {valueToWriteTB.Text.ToString()} \n");
            }
            else
                ConsolePrintTB.Text += $"Writing register failed. {reply}\n";
        }

        //UI
        private delegate void SetTextBoxPropertyCallBack(RichTextBox c, string Value);
        public static void WriteText(RichTextBox c, string Value)
        {
            if (c.InvokeRequired)
            {
                SetTextBoxPropertyCallBack d = new SetTextBoxPropertyCallBack(WriteText);
                c.BeginInvoke(d, c, Value);
            }
            else
            {
                c.AppendText(Value);
            }
        }

        


        //descriptions- mouse hover
        private void Micron_locationlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.Micron_locationlbl, "with linkage number and micron ");
        }

        private void PCS_DL_In_UL_Outlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.PCS_DL_In_UL_Outlbl, "(green port)Options: 0 = PCS 0 , 1 = PCS 1 etc.. , \n this should also configure recovery clock");
        }

        private void PCS_UL_In_DL_Out_1lbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.PCS_UL_In_DL_Out_1lbl, "(orange port1)Options: 0 = PCS 0 , 1 = PCS 1 etc.., 4 = Disable");
        }

        private void PCS_UL_In_DL_Out_2lbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.PCS_UL_In_DL_Out_2lbl, "(orange port 2)Options: 0 = PCS 0 , 1 = PCS 1 etc.., 4 = Disable");
        }

        private void DL_Sub_Channel_0lbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.DL_Sub_Channel_0lbl, "0 = 10MHz, 1 = 5MHz, 2 = 3MHz, 3 = 1.4MHz, 4 = 0MHz = bypass/configure 16 channels");
        }
        private void DLFDDCenterFrequencylbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.DLFDDCenterFrequencylbl, "KHz 18 Hz resolution   old - Sub_Channel_1 DL Center Frequency = 739; MHz ");
        }
        private void Sub_Channel_0DL_CenterFrequencyOffsetlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.Sub_Channel_0DL_CenterFrequencyOffsetlbl, "KHz = 2.5MHz  100KHz resolution   old - Sub_Channel_0 DL RF Center Frequency = 705;  MHz");
        }
        private void Local_Enlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.Local_Enlbl, "enable operation of local micron and not just pass through mode");
        }

        private void Transmit_zeros_from_BFlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.Transmit_zeros_from_BFlbl, "no data on serdes excet managment messages)");
        }

        private void ULFDDCenterFrequencylbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.ULFDDCenterFrequencylbl, "KHz 18 Hz resolution  old -Sub_Channel_0 UL RF Center Frequency = 705; MHz");
        }

        private void Sub_Channel_0UL_CenterFrequencyOffsetlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.Sub_Channel_0UL_CenterFrequencyOffsetlbl, "KHz = 2.5MHz 100KHz resolution    old - Sub_Channel_1 UL RF Center Frequency = 739; MHz");
        }
        private void UL_Sub_Channel_0_Bandwidthlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.UL_Sub_Channel_0_Bandwidthlbl, "0 = 10MHz, 1 = 5MHz, 2 = 3MHz, 3 = 1.4MHz/configure 16 channels");
        }

        private void AGCBACKOFFlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.AGCBACKOFFlbl, "dB - POSTIVE VALUES");
        }
        private void AGCaverageshiftlbl_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this.AGCaverageshiftlbl, "values between 0-16, a = 1/2^Average shift ,AvePower(n) = a * Power(n) + (1 - a )*AvePower(n-1)");
        }








        private void Main_Resize(object sender, System.EventArgs e)
        {
            buttonOpen.Location = new Point(3, blocksPanel.ClientRectangle.Height + 2);
        }

    }
}
